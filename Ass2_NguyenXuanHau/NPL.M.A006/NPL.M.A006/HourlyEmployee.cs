﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A006
{
    internal class HourlyEmployee: Employee
    {
        public double wage {  get; set; }
        public double workingHour { get; set; }

        public HourlyEmployee() { }

        public HourlyEmployee(string ssn, string firstName, string lastName, 
            DateTime birthDate, string phone, string email,
            double wage, double workingHour) : base(ssn, firstName, lastName, birthDate, phone, email)
        {
            this.wage = wage;
            this.workingHour = workingHour;
        }

        public override string? ToString()
        {
            return string.Format("{0, -10}  {1, -15} {2, -15} {3, -15} {4, -10} {5, -20} {6, -5} {7, -5} ", 
                ssn, firstName, lastName, birthDate.ToString("dd/MM/yyy"), phone, email, wage, workingHour);
        }
    }
}
