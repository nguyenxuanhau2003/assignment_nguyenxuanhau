﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A006
{
    internal abstract class Employee
    {
        public string ssn { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public DateTime birthDate { get; set; }
        public string phone {  get; set; }
        public string email { get; set; }

        public Employee() { }

        public Employee(string ssn, string firstName, string lastName)
        {
            this.ssn = ssn;
            this.firstName = firstName;
            this.lastName = lastName;
        }

        protected Employee(string ssn, string firstName, string lastName, DateTime birthDate, string phone, string email) : this(ssn, firstName, lastName)
        {
            this.birthDate = birthDate;
            this.phone = phone;
            this.email = email;
        }
    }
}
