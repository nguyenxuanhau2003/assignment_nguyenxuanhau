﻿using static System.Runtime.InteropServices.JavaScript.JSType;

static List<string> InputRecord()
{

    List<string> records = new List<string>();

    while (true)
    {
        ConsoleKeyInfo press = Console.ReadKey();

        if (press.Key == ConsoleKey.Enter && press.Modifiers == ConsoleModifiers.Control)
        {
            break;
        }
        string record = press.KeyChar + Console.ReadLine();
        records.Add(record);
    }
    return records;
}


List<string> arrInput = InputRecord();
for (int i = 0; i < arrInput.Count; i++)
{
    for (int j = i + 1; j < arrInput.Count; j++)
    {
        int startI = arrInput[i].IndexOf(" at ") + 4;
        int endI = arrInput[i].IndexOf("by") - arrInput[i].IndexOf(" at ") - 5;
        string stringDateI = arrInput[i].Substring(startI, endI);
        DateTime dateI= DateTime.ParseExact(stringDateI, "dd/MM/yyyy hh:mm:ss tt", null);

        int startJ = arrInput[j].IndexOf(" at ") + 4;
        int endJ = arrInput[j].IndexOf("by") - arrInput[j].IndexOf(" at ") - 5;
        string stringDateJ = arrInput[j].Substring(startJ, endJ);
        DateTime dateJ = DateTime.ParseExact(stringDateJ, "dd/MM/yyyy hh:mm:ss tt", null);
        
        string tmp;
        if(dateI> dateJ)
        {
            tmp = arrInput[j];
            arrInput[j] = arrInput[i];
            arrInput[i] = tmp;
        }
    }
}

for(int i = 0; i < arrInput.Count; i++)
{
    Console.WriteLine(arrInput[i]);
}
