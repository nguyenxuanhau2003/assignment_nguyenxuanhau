﻿using System.Text.RegularExpressions;
//^[A-Za-z0-9!#$%&'*+\/=?^_{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+\/=?^_{|}~-]+)*@[A-Za-z0-9-]+(?:\.[A-Za-z0-9-]+)*$

static bool IsEmail(string email)
{
    string parrtern = "^(?:[a-zA-Z0-9]+(?:[.!#$%&'*+/=?^_{|}~-][a-zA-Z0-9]+)*)@(?:[a-zA-Z](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z]{2,}$";
    return Regex.IsMatch(email, parrtern);
}

string email= Console.ReadLine();
if (IsEmail(email))
{
    Console.WriteLine("Email is valid"); 

}
else
{
    Console.WriteLine("Email is invalid");
}