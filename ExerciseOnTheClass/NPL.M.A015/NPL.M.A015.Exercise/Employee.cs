﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class Employee
    {
        public int EmployeeId { get; set; }
        public string EmployeeName {  get; set; }
        public int Age {  get; set; }
        public string Address {  get; set; }
        public DateTime HiredDate { get; set; }
        public bool Status {  get; set; }
        public int DepartmentId { get; set; }

        public Employee()
        {
        }

        public Employee(int employeeId, string employeeName, int age, string address, DateTime hiredDate, bool status, int departmentId)
        {
            EmployeeId = employeeId;
            EmployeeName = employeeName;
            Age = age;
            Address = address;
            HiredDate = hiredDate;
            Status = status;
            DepartmentId = departmentId;
        }
    }
}
