﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class ProgramingLanguage
    {
        public string LanguageName {  get; set; }

        public ProgramingLanguage()
        {
        }

        public ProgramingLanguage(string languageName)
        {
            LanguageName = languageName;
        }
    }
}
