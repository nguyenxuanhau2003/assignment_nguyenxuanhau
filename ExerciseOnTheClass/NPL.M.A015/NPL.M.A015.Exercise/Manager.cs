﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.Intrinsics.Arm;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace NPL.M.A015.Exercise
{
    internal class Manager
    {
        Validation validation = new Validation();
        List<Department> departments = new List<Department>()
        {
            new Department(1, "Cong nghe thong tin"),
            new Department(2, "Kinh te"),
            new Department(3, "Thiet ke do hoa"),
            new Department(4, "Ngon ngu")
        };
        List<Employee> employees = new List<Employee>()
        {
            new Employee(1, "Nguyen Tuan Ninh", 19, "Ha Noi", DateTime.ParseExact("13/11/2003", "dd/MM/yyyy", null), true, 1),
            new Employee(2, "Nguyen Xuan Hau", 20, "Ha Noi", DateTime.ParseExact("13/11/2003", "dd/MM/yyyy", null), true, 1),
            new Employee(3, "Nguyen Hai Nam", 19, "Ha Nam", DateTime.ParseExact("13/11/2003", "dd/MM/yyyy", null), true, 2),
            new Employee(4, "Dinh Duc Minh", 19, "Ha Noi", DateTime.ParseExact("13/11/2003", "dd/MM/yyyy", null), false, 2),
            new Employee(5, "Hoang Tuan Minh", 19, "Ha Noi", DateTime.ParseExact("13/11/2003", "dd/MM/yyyy", null), true, 3),
            new Employee(6, "Dieu Quy Duy", 19, "Ha Noi", DateTime.ParseExact("13/11/2003", "dd/MM/yyyy", null), true, 4)
        };
        List<ProgramingLanguage> programingLanguages = new List<ProgramingLanguage>()
        {
            new ProgramingLanguage("C#"),
            new ProgramingLanguage("Java"),
            new ProgramingLanguage("Javascript"),
            new ProgramingLanguage("C/C++")
        };
        List<EmployeeLanguage> employeeLanguages = new List<EmployeeLanguage>()
        {
            new EmployeeLanguage(1, "C/C++"),
            new EmployeeLanguage(1, "Java"),
            new EmployeeLanguage(2, "C/C++"),
            new EmployeeLanguage(3, "C#"),
            new EmployeeLanguage(3, "Javascript"),
            new EmployeeLanguage(4, "C#"),
            new EmployeeLanguage(4, "C#"),
            new EmployeeLanguage(5, "C#"),
            new EmployeeLanguage(6, "C#")
        };

        // Trả về các departments có số lượng nhân viên lớn hơn numberOfEmployee
        public List<Department> GetDepartments(int numberOfEmployees)
        {
            List<Department> result = new List<Department>();
            var getDepartments = from dp in departments
                                 join em in employees on dp.DepartmentId equals em.DepartmentId into studentsInADepartment
                                 where studentsInADepartment.Count() >= numberOfEmployees
                                 select dp;
            return getDepartments.ToList();
        }

        // Trả về danh sách nhân viên đang làm việc.
        public List<Employee> GetEmployeesWorking()
        {
            return employees.Where(x => x.Status == true).ToList();
        }

        // Trả về danh sách tất cả các nhân viên biết languageName
        public List<Employee> GetEmployees(string languageName)
        {
            var GetEmployees = from emp in employees
                               join stlg in employeeLanguages on emp.EmployeeId equals stlg.EmployeeId
                               where stlg.LanguageName == languageName
                               select emp;
            return GetEmployees.ToList();
        }

        // Trả về ngôn ngữ mà nhân viên có mã employId biết
        public List<ProgramingLanguage> GetLanguages(int employeeId)
        {
            var getLanguages = from stlg in employeeLanguages
                               join lg in programingLanguages on stlg.LanguageName equals lg.LanguageName
                               where stlg.EmployeeId == employeeId
                               select lg;
            return getLanguages.ToList();
        }

        // Lấy nhân viên biết nhiều hơn 1 ngôn ngữ
        public List<Employee> GetSeniorEmployee()
        {
            var getSeniorEmployee = from emp in employees
                                    join emplg in employeeLanguages on emp.EmployeeId equals emplg.EmployeeId into numberOfLanguage
                                    where numberOfLanguage.Count() > 1
                                    select emp;
            return getSeniorEmployee.ToList();

        }

        // trả về danh sách nhân viên trong đó pageIndex là trang hiện tại,
        // pageSize là kích thước của trang,tên nhân viên là tên nhân viên muốn tìm kiếm
        // và đặt hàng=”ASC” hoặc “DESC” đó trả về danh sách nhân viên được sắp xếp tăng dần hoặc giảm dần.
        public List<Employee> GetEmployeePaging(int pageIndex = 1, int pageSize = 10, string employeeName = null, string order = "ASC")
        {
            // Lấy start index và end index
            int startIndex = (pageIndex - 1) * pageSize;
            int endIndex = startIndex + pageSize < employees.Count ? startIndex + pageSize : employees.Count - 1;

            // lấy danh sách sinh viên có index từ start index đến end index
            List<Employee> employeeToSelect = new List<Employee>();
            for (int i = startIndex; i < endIndex; i++)
            {
                employeeToSelect.Add(employees[i]);
            }

            // trả về kết quả danh sách sinh viên sắp xếp theo thứ tự tên. Nếu không có sinh viên nào thoả mãn thì hàm trả về list rỗng.
            if (order.Equals("DESC"))
                return employeeToSelect.Where(x => x.EmployeeName.Contains(employeeName)).OrderByDescending(x => x.EmployeeName).ToList();
            else
                return employeeToSelect.Where(x => x.EmployeeName.Contains(employeeName)).OrderBy(x => x.EmployeeName).ToList();
        }

        // trả về tất cả các phòng ban bao gồm cả nhân viên thuộc từng phòng ban.
        public Dictionary<Department, List<Employee>> GetDepartments()
        {
            Dictionary<Department, List<Employee>> result = new Dictionary<Department, List<Employee>>();
            var getDepartments = from dp in departments
                                 join emp in employees on dp.DepartmentId equals emp.DepartmentId into employeeInDepartment
                                 select new
                                 {
                                     department = dp,
                                     employeeList = employeeInDepartment
                                 };
            foreach (var d in getDepartments)
            {
                result.Add(d.department, d.employeeList.ToList());
            }
            return result;
        }


        // Nhập Department
        public void InputDepartment()
        {
            do
            {
                Department department = new Department();
                bool check = true;
                int id;
                do
                {
                    check = true;
                    id = validation.InputPostiveInteger("Input Department ID: ");
                    if (departments.Where(x => x.DepartmentId == id).Count() != 0)
                    {
                        check = false;
                        Console.WriteLine("Department ID đã tổn tại!");
                        continue;
                    }
                } while (check == false);
                department.DepartmentId = id;
                department.DepartmentName = validation.InputString("Department Name: ", "^[A-Za-zÀ-ỹ\\s]+$");
                departments.Add(department);
            }
            while (validation.InputYesNo("Do you want to input more? (Y/y or N|n): "));
        }
        // Nhập nhân viên
        public void InputEmployee()
        {
            do
            {
                Employee employee = new Employee();
                int id = 0;
                int countDp = 0;
                bool check = true;
                do
                {
                    check = true;
                    id = validation.InputPostiveInteger("Input Employee ID: ");
                    if (employees.Where(x => x.EmployeeId == id).Count() != 0)
                    {
                        check = false;
                        Console.WriteLine("Employee ID đã tổn tại!");
                        continue;
                    }
                } while (check == false);
                employee.EmployeeId = id;
                employee.EmployeeName = validation.InputString("Employee Name: ", "^[A-Za-zÀ-ỹ\\s]+$");
                employee.Age = validation.InputPostiveInteger("Input age: ");
                employee.Address = validation.InputString("Employee Address: ", "^[A-Za-zÀ-ỹ\\s]+$");
                employee.HiredDate = validation.InputDate("Hired Date (dd/MM/yyyy): ");
                employee.Status = validation.InputYesNo("Enter status (Y/N): ");
                int departmentId;
                do
                {
                    departmentId = validation.InputPostiveInteger("Input Department ID: ");
                    if (departments.Where(x => x.DepartmentId == departmentId).Count() == 1)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Department ID doesn't exist!");
                    }
                } while (true);
                employee.DepartmentId = departmentId;
                employees.Add(employee);
            } while (validation.InputYesNo("Do you want to input more (Y/y or N/n): "));
        }
        // Nhập programmingLanguage
        public void InputProgramingLanguage()
        {
            do
            {
                ProgramingLanguage programingLanguage = new ProgramingLanguage();
                string name = "";
                int countDp = 0;

                do
                {
                    Console.Write("Input programming language: ");
                    name = Console.ReadLine();
                    if (programingLanguages.Where(x => x.LanguageName.Equals(name)).Count() == 1)
                    {
                        Console.WriteLine("Programming language existed!");
                        continue;
                    }
                    else
                    {
                        break;
                    }
                } while (true);
                programingLanguage.LanguageName = name;
                programingLanguages.Add(programingLanguage);
            } while (validation.InputYesNo("Do you want to input more (Y/y or N/n): "));
        }



        // Nhập nhân viên - ngôn ngữ
        public void InputLanguageEmployee()
        {
            do
            {
                string name = "";
                do
                {
                    Console.Write("Input language name: ");
                    name = Console.ReadLine();
                    if (programingLanguages.Where(x => x.LanguageName == name).Count() == 1)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Programming Language doesn't exist");
                        continue;
                    }

                } while (true);

                int id = 0;
                do
                {
                    id = validation.InputPostiveInteger("Input Employee ID: ");
                    if (employees.Where(x => x.EmployeeId == id).Count() == 1)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Employee ID doesn't exist!");
                    }
                } while (true);
                if (employeeLanguages.Where(x => x.EmployeeId == id).Count() == 1 && employeeLanguages.Where(x => x.LanguageName == name).Count() == 1)
                {
                    Console.WriteLine("Employee-Language exists");
                }
                else
                {
                    EmployeeLanguage languageEmployee = new EmployeeLanguage(id, name);
                    employeeLanguages.Add(languageEmployee);
                }
            } while (validation.InputYesNo("Do you want to input more (Y/y or N/n): "));
        }

    }
}
