﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Exercise2
{
    
    public static class ArrayExtension
    {
        //function returns the array of distinct elements.
        public static int[] RemoveDuplicate(this int[] array)
        {
            List<int> resultList = new List<int>();
            int count = 0;
            foreach(int i in array)
            {
                if (!resultList.Contains(i))
                {
                    resultList.Add(i);
                }
            }
            return resultList.ToArray();
        }

        //function returns the array of distinct elements.
        public static T[] RemoveDuplicate<T>(this T[] array)
        {
            List<T> resultList = new List<T>();
            int count = 0;
            foreach (T i in array)
            {
                if (!resultList.Contains(i))
                {
                    resultList.Add(i);
                }
            }
            return resultList.ToArray();
        }
    }
}
