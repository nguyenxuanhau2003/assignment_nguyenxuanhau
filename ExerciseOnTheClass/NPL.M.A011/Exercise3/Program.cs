﻿// See https://aka.ms/new-console-template for more information
using Exercise3;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;

int[] ints = { 1, 2, 3, 5, 7, 3, 2 };
int viTri1 = ints.LastIndexOf(3);
Console.WriteLine("Chỉ số cuối cùng của số 3 trong mảng số nguyên: " + viTri1);

string[] ten = { "Hoàng", "Trọng", "A", "Hoàng", "Trọng", "Hiếu" };
int viTri2 = ten.LastIndexOf("Trọng");
Console.WriteLine("Chỉ số cuối cùng của 'Trọng' trong mảng tên: " + viTri2);
