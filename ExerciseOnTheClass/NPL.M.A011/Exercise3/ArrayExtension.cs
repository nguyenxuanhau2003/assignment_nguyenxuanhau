﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise3
{
    internal static class ArrayExtension
    {
        //returns the last index of element has value equal elementValue in the array.
        public static int LastIndexOf<T>(this T[] array, T elementValue) where T : IComparable<T>
        {
            for (int i = array.Length - 1; i >= 0; i--)
            {
                if (object.Equals(array[i], elementValue))
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
