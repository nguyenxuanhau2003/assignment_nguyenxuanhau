﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace NPL.M.A010.Exercise
{
    internal class Manager
    {
        List<OutlookMail> listMail = new List<OutlookMail>();
        Validation validation = new Validation();
        public void NewMail()
        {
            while (true)
            {
                Console.WriteLine("==Enter email information==");
                string from = validation.InputEmail("From: ");
                string[] to = validation.InputArrayEmail("To: ");
                string[] cc = validation.InputArrayEmail("Cc: ");
                string subject = validation.InputString("Subject: ");
                string attachment = validation.InputString("Attachment: ");

                Console.WriteLine("Mail body: ");
                string MailBody = "";
                while (true)
                {
                    string line = Console.ReadLine();
                    if (line.Equals("<END>"))
                        break;

                    MailBody += line + "\n";
                }

                Console.WriteLine("Is important");
                bool isImportant = validation.CheckInputYN();
                string password = null;
                if (isImportant)
                {
                   
                    Console.WriteLine("Password: ");
                    password = Console.ReadLine();
                    password = validation.HashPasswordSHA256(password);
                }

                Console.WriteLine("====Mailing Menu====");
                Console.WriteLine("1. Sent");
                Console.WriteLine("2. Draft");
                Console.WriteLine("3. Re-Enter");
                Console.WriteLine("4. Main menu");
                EmailStatus emailStatus;
                int choice = validation.InputIntegerInRange("Nhập lựa chọn: ", 1, 4);
                if (choice == 1 || choice == 2)
                {
                    emailStatus = (choice == 1) ? EmailStatus.Sent : EmailStatus.Draft;
                    string message = (choice == 1) ? "Email is sent successfully!" : "Email is save draft successfully!";
                    OutlookMail outlookMail = new OutlookMail(from, to, cc, subject, attachment, MailBody, isImportant, password, emailStatus, DateTime.Now);
                    listMail.Add(outlookMail);
                    WriteFile(outlookMail);
                    Console.WriteLine(message);
                    Console.WriteLine("======================================");
                    Console.WriteLine("Do you want sent another mail (yes/no)?");
                    if (!validation.CheckInputYN())
                    {
                        break;
                    }
                }
                else if (choice == 4)
                {
                    break;
                }
            }

        }

        public void Sent()
        {
            foreach (OutlookMail mail in listMail)
            {
                if (mail.Status == EmailStatus.Sent)
                {
                    mail.DisplayMail();
                }
            }
        }
        public void Draft()
        {
            foreach (OutlookMail mail in listMail)
            {
                if (mail.Status == EmailStatus.Draft)
                {
                    mail.DisplayMail();
                }
            }
        }


        public void WriteFile(OutlookMail outlookMail)
        {
            string xmlFilePath = @"D:\FPTmaterials\Term6\C#\assignment_nguyenxuanhau\BaiTrenLop\NPL.M.A010\NPL.M.A010.Exercise\OutlookEmail.xml";

            XmlDocument doc = new XmlDocument();

            XmlElement root = doc.CreateElement("OutlookEmail");
            doc.AppendChild(root);

            //add Exit data
            try
            {
                XmlDocument docExit = new XmlDocument();
                docExit.Load(xmlFilePath);

                XmlNode outlookEmailNode = docExit.SelectSingleNode("OutlookEmail");

                if (outlookEmailNode != null)
                {
                    XmlNodeList mailNodes = outlookEmailNode.SelectNodes("Mail");
                    foreach (XmlNode mailNode in mailNodes)
                    {
                        XmlElement mailElementExit = doc.CreateElement("Mail");
                        root.AppendChild(mailElementExit);

                        string from = mailNode.SelectSingleNode("From").InnerText;
                        AddXmlElement(doc, mailElementExit, "From", from);

                        XmlElement ToElementExit = doc.CreateElement("To");
                        XmlNodeList toNodes = mailNode.SelectNodes("To/Address");
                        foreach (XmlNode toNode in toNodes)
                        {
                            string toAddress = toNode.InnerText;
                            AddXmlElement(doc, ToElementExit, "Address", toAddress);
                        }
                        mailElementExit.AppendChild(ToElementExit);

                        XmlElement CcElementExit = doc.CreateElement("Cc");
                        XmlNodeList ccNodes = mailNode.SelectNodes("Cc/Address");
                        foreach (XmlNode ccNode in ccNodes)
                        {
                            string ccAddress = ccNode.InnerText;
                            AddXmlElement(doc, CcElementExit, "Address", ccAddress);
                        }
                        mailElementExit.AppendChild(CcElementExit);

                        string subject = mailNode.SelectSingleNode("Subject").InnerText;
                        string attachment = mailNode.SelectSingleNode("Attachment").InnerText;
                        string body = mailNode.SelectSingleNode("Body").InnerText;
                        string isImportant = mailNode.SelectSingleNode("IsImportant").InnerText;
                        string password = mailNode.SelectSingleNode("Password").InnerText;
                        string status = mailNode.SelectSingleNode("Status").InnerText;
                        string sentDate = mailNode.SelectSingleNode("SentDate").InnerText;
                        AddXmlElement(doc, mailElementExit, "Subject", subject);
                        AddXmlElement(doc, mailElementExit, "Attachment", attachment);
                        AddXmlElement(doc, mailElementExit, "Body", body);
                        AddXmlElement(doc, mailElementExit, "IsImportant", isImportant);
                        AddXmlElement(doc, mailElementExit, "Password", password);
                        AddXmlElement(doc, mailElementExit, "Status", status);
                        AddXmlElement(doc, mailElementExit, "SentDate", sentDate);
                    }
                }
                else
                {
                    Console.WriteLine("OutlookEmail node not found.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"An error occurred: {e.Message}");
            }

            XmlElement mailElement = doc.CreateElement("Mail");
            root.AppendChild(mailElement);

            AddXmlElement(doc, mailElement, "From", outlookMail.From);

            XmlElement ToElement = doc.CreateElement("To");
            foreach (var toAddress in outlookMail.To)
            {
                AddXmlElement(doc, ToElement, "Address", toAddress);
            }
            mailElement.AppendChild(ToElement);

            XmlElement CcElement = doc.CreateElement("Cc");
            foreach (var ccAddress in outlookMail.Cc)
            {
                AddXmlElement(doc, CcElement, "Address", ccAddress);
            }
            mailElement.AppendChild(CcElement);

            AddXmlElement(doc, mailElement, "Subject", outlookMail.Subject);
            AddXmlElement(doc, mailElement, "Attachment", outlookMail.Attachment);
            AddXmlElement(doc, mailElement, "Body", outlookMail.MailBody);
            AddXmlElement(doc, mailElement, "IsImportant", outlookMail.IsImportant ? "Yes" : "No");
            AddXmlElement(doc, mailElement, "Password", outlookMail.Password);
            AddXmlElement(doc, mailElement, "Status", outlookMail.Status.ToString());
            AddXmlElement(doc, mailElement, "SentDate", outlookMail.SendDate.ToString("yyyy/MM/dd hh:mm tt"));

            doc.Save(xmlFilePath);

        }

        public void ReadFile()
        {
            string xmlFilePath = @"D:\FPTmaterials\Term6\C#\assignment_nguyenxuanhau\BaiTrenLop\NPL.M.A010\NPL.M.A010.Exercise\OutlookEmail.xml";

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(xmlFilePath);

                XmlNode outlookEmailNode = doc.SelectSingleNode("OutlookEmail");

                if (outlookEmailNode != null)
                {
                    XmlNodeList mailNodes = outlookEmailNode.SelectNodes("Mail");
                    foreach (XmlNode mailNode in mailNodes)
                    {
                        string from = mailNode.SelectSingleNode("From").InnerText;
                        Console.WriteLine("From: " + from);

                        XmlNodeList toNodes = mailNode.SelectNodes("To/Address");
                        foreach (XmlNode toNode in toNodes)
                        {
                            string toAddress = toNode.InnerText;
                            Console.WriteLine("To: " + toAddress);
                        }

                        XmlNodeList ccNodes = mailNode.SelectNodes("Cc/Address");
                        foreach (XmlNode ccNode in ccNodes)
                        {
                            string ccAddress = ccNode.InnerText;
                            Console.WriteLine("Cc: " + ccAddress);
                        }

                        string subject = mailNode.SelectSingleNode("Subject").InnerText;
                        string attachment = mailNode.SelectSingleNode("Attachment").InnerText;
                        string body = mailNode.SelectSingleNode("Body").InnerText;
                        string isImportant = mailNode.SelectSingleNode("IsImportant").InnerText;
                        string password = mailNode.SelectSingleNode("Password").InnerText;
                        string status = mailNode.SelectSingleNode("Status").InnerText;
                        string sentDate = mailNode.SelectSingleNode("SentDate").InnerText;

                        Console.WriteLine($"Subject: {subject}");
                        Console.WriteLine($"Attachment: {attachment}");
                        Console.WriteLine($"Body: {body}");
                        Console.WriteLine($"IsImportant: {isImportant}");
                        Console.WriteLine($"Password: {password}");
                        Console.WriteLine($"Status: {status}");
                        Console.WriteLine($"Sent Date: {sentDate}");
                        Console.WriteLine("----------------------");
                    }
                }
                else
                {
                    Console.WriteLine("OutlookEmail node not found.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"An error occurred: {e.Message}");
            }

        }

        private static void AddXmlElement(XmlDocument doc, XmlElement parentElement, string elementName, string elementValue)
        {
            XmlElement element = doc.CreateElement(elementName);
            element.InnerText = elementValue;
            parentElement.AppendChild(element);
        }

    }
}
