﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A010.Exercise
{
    internal class OutlookMail
    {
        public string From { get; set; }
        public string[] To { get; set; }
        public string[] Cc { get; set; }
        public string Subject { get; set; }
        public string Attachment { get; set; }
        public string MailBody { get; set; }
        public bool IsImportant { get; set; }
        public string Password { get; set; }
        public EmailStatus Status { get; set; }
        public DateTime SendDate { get; set; }

        public OutlookMail() { }

        public OutlookMail(string from, string[] to, string[] cc, string subject, string attachment, string mailBody, bool isImportant, string password, EmailStatus status, DateTime sendDate = new DateTime())
        {
            From = from;
            To = to;
            Cc = cc;
            Subject = subject;
            Attachment = attachment;
            MailBody = mailBody;
            IsImportant = isImportant;
            Password = password;
            Status = status;
            SendDate = sendDate;
        }

        public void DisplayMail()
        {
            Console.WriteLine("From: " + From);
            Console.Write("To: ");
            for (int i = 0; i < To.Length; i++)
            {
                if (i < To.Length - 1)
                {
                    Console.Write(To[i] + ",");
                }
                else { Console.Write(To[i]); }
            }
            Console.WriteLine();
            Console.WriteLine("Subject: " + Subject);
        }


    }
}
