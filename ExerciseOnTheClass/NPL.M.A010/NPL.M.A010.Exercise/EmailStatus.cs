﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A010.Exercise
{
    internal enum EmailStatus
    {
        Sent, Draft
    }
}
