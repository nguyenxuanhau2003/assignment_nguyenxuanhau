﻿using NPL.M.A010.Exercise;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;

Validation validation = new Validation();

Manager manager = new Manager();
int choice;
do
{
    Console.WriteLine("======Assignment 10- Ooutlook Emulator=========");
    Console.WriteLine("Please select");
    Console.WriteLine("1. New Mail");
    Console.WriteLine("2. Sent");
    Console.WriteLine("3. Draft");
    Console.WriteLine("4. ReadFile");
    Console.WriteLine("5.Exit");
    choice = validation.InputIntegerInRange("Nhập lựa chọn: ", 1, 5);

    switch (choice)
    {
        case 1:
            manager.NewMail();
            break;
        case 2:
            manager.Sent();
            break;
        case 3:
            manager.Draft();
            break;
        case 4:
            manager.ReadFile();
            break;
        case 5:
            Console.WriteLine("Kết thúc chương trình!");
            break;
        default:
            Console.WriteLine("Không có lựa chọn!");
            break;
    }
}
while (choice >= 0 && choice <= 4);

