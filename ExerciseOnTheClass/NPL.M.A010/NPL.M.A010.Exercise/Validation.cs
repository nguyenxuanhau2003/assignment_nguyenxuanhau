﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NPL.M.A010.Exercise
{
    internal class Validation
    {
        public string InputString(string message, string pattern)
        {
            string input;
            while (true)
            {
                Console.WriteLine(message);
                input = Console.ReadLine();
                if (!Regex.IsMatch(input, pattern) || input.Equals(""))
                {
                    Console.WriteLine("Nhập chuỗi phù hợp regex");
                    continue;
                }
                return input;
            }
        }

        public  string InputString(string message)
        {
            Console.WriteLine(message);
            string input = Console.ReadLine();
            return input;
        }

        public int InputIntegerInRange(string message, int min, int max)
        {
            string input;
            int result;
            while (true)
            {
                try
                {
                    Console.Write(message);
                    input = Console.ReadLine();
                    result = Convert.ToInt32(input);
                    if (result < min || result > max)
                    {
                        Console.WriteLine("Invalid input!");
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Console.Write("Invalid input!");
                }
            }
            return result;
        }


        public  string InputEmail(string message)
        {
            string pattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$";
            string input;
            while (true)
            {
                Console.WriteLine(message);
                input = Console.ReadLine();
                if (!Regex.IsMatch(input, pattern) || input.Equals(""))
                {
                    Console.WriteLine("Email không đúng định dạng!");
                    continue;
                }
                return input;
            }
        }

        public  bool CheckEmailValid(string input)
        {
            string pattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$";
            if (!Regex.IsMatch(input, pattern) || input.Equals(""))
            {
                return false;
            }
            return true;
        }

        public  string[] InputArrayEmail(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                string toInput = Console.ReadLine();
                string[] toArr = toInput.Split(",");
                for (int i = 0; i < toArr.Length; i++)
                {
                    toArr[i] = toArr[i].Trim();
                    if (!CheckEmailValid(toArr[i]))
                    {
                        Console.WriteLine("Email chưa đúng");
                        continue;
                    }
                    return toArr;
                }
            }
        }

        public  string HashPasswordSHA256(string password)
        {
            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] data = Encoding.UTF8.GetBytes(password);
                byte[] hash = sha256.ComputeHash(data);

                // Convert the byte array to a hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("x2"));
                }

                return sb.ToString();
            }
        }



        public bool CheckInputYN()
        {
            while (true)
            {
                string result = Console.ReadLine();
                if (result.Equals("Y") || result.Equals("y"))
                {
                    return true;
                }
                else if (result.Equals("N") || result.Equals("n"))
                {
                    return false;
                }
                Console.WriteLine("Please input y/Y or n/N.");
                Console.WriteLine("Enter again: ");
            }
        }

    }
}
