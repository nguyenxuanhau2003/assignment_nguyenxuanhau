﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20_10_linq
{
    internal class MonHoc
    {
        public int MaMonHoc { get; set; }
        public string TenMonHoc { get; set; }

        public MonHoc()
        {
        }

        public MonHoc(int maMonHoc, string tenMonHoc)
        {
            MaMonHoc = maMonHoc;
            TenMonHoc = tenMonHoc;
        }
    }
}
