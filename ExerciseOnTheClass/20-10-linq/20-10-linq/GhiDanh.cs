﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20_10_linq
{
    internal class GhiDanh
    {
        public int MaSinhVien { get; set; }
        public int MaMonHoc { get; set; }
        public double Diem { get; set; }

        public GhiDanh()
        {
        }

        public GhiDanh(int maSinhVien, int maMonHoc, double diem)
        {
            MaSinhVien = maSinhVien;
            MaMonHoc = maMonHoc;
            Diem = diem;
        }
    }
}
