﻿using _20_10_linq;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;


Validation validation = new Validation();
QuanLy quanLy = new QuanLy();
quanLy.Init();
bool exit = false;

while (!exit)
{
    Console.WriteLine("Menu:");
    Console.WriteLine("1. Thống kê danh sách sinh viên và số lượng môn học");
    Console.WriteLine("2. Tính điểm trung bình của sinh viên");
    Console.WriteLine("3. Thống kê môn học có bao nhiêu sinh viên");
    Console.WriteLine("4. Thống kê số lượng sinh viên");
    Console.WriteLine("5. Thống kê số lượng môn học có sinh viên");
    Console.WriteLine("6. Thống kê số lượng môn học không có sinh viên");
    Console.WriteLine("7. Nhập thông tin sinh viên");
    Console.WriteLine("8. Nhập thông tin môn học");
    Console.WriteLine("9. Nhập thông tin ghi danh");
    Console.WriteLine("10. Thoát");

    int choice = validation.InputPostiveInteger("Nhập lựa chọn (1-10): ");

    switch (choice)
    {
        case 1:
            // Thống kê danh sách sinh viên và số lượng môn học
            quanLy.ThongKeDanhSachVaTongSoMonHocCuaTungSV();
            break;
        case 2:
            // Thêm điểm và tính điểm trung bình của sinh viên
            quanLy.TinhDiemTB();
            break;
        case 3:
            // Thống kê môn học có bao nhiêu sinh viên
            quanLy.ThongKeMonHocCoBaoNhieuSinhVien();
            break;
        case 4:
            // Thống kê số lượng sinh viên
            quanLy.ThongKeSoLuongSinhVien();
            break;
        case 5:
            // Thống kê số lượng môn học có sinh viên
            quanLy.ThongKeSoLuongMonHocCoSinhVien();
            break;
        case 6:
            // Thống kê số lượng môn học không có sinh viên
            quanLy.ThongKeSoLuongMonHocKhongCoSinhVien();
            break;
        case 7:
            // Nhập thông tin sinh viên, môn học, ghi danh
            quanLy.NhapDanhSachSinhVien();
            break;
        case 8:
            // Nhập thông tin sinh viên, môn học, ghi danh
            quanLy.NhapDanhSachMonHoc();
            break;
        case 9:
            // Nhập thông tin sinh viên, môn học, ghi danh
            quanLy.NhapDanhSachGhiDanh();
            break;
        case 10:
            exit = true;
            break;
        default:
            Console.WriteLine("Chức năng không hợp lệ. Hãy chọn lại.");
            break;
    }
}
Console.ReadKey();