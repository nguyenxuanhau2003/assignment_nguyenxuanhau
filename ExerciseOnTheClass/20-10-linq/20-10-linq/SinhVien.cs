﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20_10_linq
{
    internal class SinhVien
    {
        public int MaSinhVien { get; set; }
        public string HoTen { get; set; }
        public int Tuoi { get; set; }
        public string Khoa { get; set; }

        public SinhVien()
        {
        }

        public SinhVien(int maSinhVien, string hoTen, int tuoi, string khoa)
        {
            MaSinhVien = maSinhVien;
            HoTen = hoTen;
            Tuoi = tuoi;
            Khoa = khoa;
        }
    }
}
