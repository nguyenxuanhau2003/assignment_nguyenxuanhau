﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace _20_10_linq
{
    internal class QuanLy
    {
        Validation validation = new Validation();
        List<SinhVien> SinhViens = new List<SinhVien>();
        List<MonHoc> MonHocs = new List<MonHoc>();
        List<GhiDanh> GhiDanhs = new List<GhiDanh>();

        public void Init()
        {
            SinhViens = new List<SinhVien>
        {
            new SinhVien { MaSinhVien = 1, HoTen = "Nguyễn Văn A", Tuoi = 20, Khoa = "Công nghệ thông tin" },
            new SinhVien { MaSinhVien = 2, HoTen = "Trần Thị B", Tuoi = 22, Khoa = "Toán học" },
            new SinhVien { MaSinhVien = 3, HoTen = "Lê Văn C", Tuoi = 21, Khoa = "Công nghệ thông tin" },
            new SinhVien { MaSinhVien = 4, HoTen = "Phạm Văn D", Tuoi = 23, Khoa = "Vật lý" },
            new SinhVien { MaSinhVien = 5, HoTen = "Hoàng Thị E", Tuoi = 24, Khoa = "Toán học" }
        };

            MonHocs = new List<MonHoc>
        {
            new MonHoc { MaMonHoc = 101, TenMonHoc = "Lập trình cơ bản" },
            new MonHoc { MaMonHoc = 102, TenMonHoc = "Toán cao cấp" },
            new MonHoc { MaMonHoc = 103, TenMonHoc = "Vật lý cơ bản" }
        };

            GhiDanhs = new List<GhiDanh>
        {
            new GhiDanh { MaSinhVien = 1, MaMonHoc = 101 , Diem = 7},
            new GhiDanh { MaSinhVien = 1, MaMonHoc = 102 , Diem = 8},
            new GhiDanh { MaSinhVien = 2, MaMonHoc = 101 , Diem = 6},
            new GhiDanh { MaSinhVien = 3, MaMonHoc = 103 , Diem = 7},
            new GhiDanh { MaSinhVien = 4, MaMonHoc = 102 , Diem = 9},
            new GhiDanh { MaSinhVien = 4, MaMonHoc = 103 , Diem = 6}
        };
        }
        public int NhapMaSinhVien()
        {
            int MaSinhVien;
            do
            {
                MaSinhVien = validation.InputPostiveInteger("Nhập mã sinh viên: ");
                if (SinhViens.Where(x => x.MaSinhVien == MaSinhVien).Count() == 1)
                {
                    Console.WriteLine("Mã sinh viên đã tồn tại!");
                    continue;
                }
                break;
            }
            while (true);
            return MaSinhVien;
        }

        public void NhapDanhSachSinhVien()
        {
            do
            {
                int MaSinhVien = NhapMaSinhVien();
                string HoTen = validation.InputString("Nhập họ tên: ", "^[a-zA-ZÀ-ỹ\\s]+$");
                int Tuoi = validation.InputPostiveInteger("Nhập tuổi sinh viên: ");
                string Khoa = validation.InputString("Nhập khoa: ", "^[a-zA-ZÀ-ỹ\\s]+$");
                SinhViens.Add(new SinhVien(MaSinhVien, HoTen, Tuoi, Khoa));
            }
            while (validation.InputYesNo("Có muốn nhập tiếp không? (Y/y N/n): "));
        }

        public int NhapMaMonHoc()
        {
            int MaMonHoc;
            do
            {
                MaMonHoc = validation.InputPostiveInteger("Nhập mã môn học: ");
                if (MonHocs.Where(x => x.MaMonHoc == MaMonHoc).Count() == 1)
                {
                    Console.WriteLine("Mã môn học đã tồn tại!");
                    continue;
                }
                break;
            }
            while (true);
            return MaMonHoc;
        }
        public void NhapDanhSachMonHoc()
        {
            do
            {
                int MaMonHoc = NhapMaMonHoc();
                string Ten = validation.InputString("Nhập họ tên: ", "^[a-zA-ZÀ-ỹ\\s]+$");
                MonHocs.Add(new MonHoc(MaMonHoc, Ten));
            }
            while (validation.InputYesNo("Có muốn nhập tiếp không? (Y/y N/n): "));
        }

        public bool CheckMaGhiDanh(int MaSinhVien, int MaMonHoc)
        {
            if (GhiDanhs.Where(x => x.MaSinhVien == MaSinhVien).Count() == 1 && GhiDanhs.Where(x => x.MaMonHoc == MaMonHoc).Count() == 1)
            {
                Console.WriteLine("Mã ghi danh đã tồn tại!");
                return false;
            }
            if (SinhViens.Where(x => x.MaSinhVien == MaSinhVien).Count() == 0)
            {
                Console.WriteLine("Mã sinh viên không tồn tại!");
                return false; ;
            }
            if (MonHocs.Where(x => x.MaMonHoc == MaMonHoc).Count() == 0)
            {
                Console.WriteLine("Mã môn học không tồn tại!");
                return false;
            }
            return true;
        }



        public void NhapDanhSachGhiDanh()
        {
            while (true)
            {
                int MaSinhVien = validation.InputPostiveInteger("Nhập mã sinh viên: ");
                int MaMonHoc = validation.InputPostiveInteger("Nhập mã môn học: ");
                double Diem = validation.InputNoNegativeDouble("Nhập điểm môn học: ");
                if(CheckMaGhiDanh(MaSinhVien, MaMonHoc))
                {
                    GhiDanhs.Add(new GhiDanh(MaSinhVien, MaMonHoc, Diem));
                    return;
                }
            }
        }

        public void ThongKeDanhSachVaTongSoMonHocCuaTungSV()
        {
            var dsSV = from sv in SinhViens
                       join gd in GhiDanhs on sv.MaSinhVien equals gd.MaSinhVien into j
                       select new
                       {
                           SinhVien = sv.HoTen,
                           SoLuongMonHoc = j.Count()
                       };
            foreach (var item in dsSV)
            {
                Console.WriteLine("Tên SV: " + item.SinhVien + " | Số lượng môn học sinh viên đó tham gia: " + item.SoLuongMonHoc);
            }

        }
        public void TinhDiemTB()
        {

            int maSinhVien = validation.InputPostiveInteger("Nhập mã SV muốn tính điểm TB: ");
            var sinhVienCanTinh = from sv in SinhViens
                                  where sv.MaSinhVien == maSinhVien
                                  select sv;
            if (sinhVienCanTinh.Count() == 1)
            {
                double diemTrungBinh = (from gd in GhiDanhs
                                        where gd.MaSinhVien == maSinhVien
                                        select gd.Diem).Average();

                Console.WriteLine("Sinh Viên có mã: " + maSinhVien + " | Điểm TB: " + diemTrungBinh);
            }
            else
            {
                Console.WriteLine("Không tìm  thấy sinh viên");
            }

        }
        public void ThongKeMonHocCoBaoNhieuSinhVien()
        {
            var query = from mh in MonHocs
                        join gd in GhiDanhs on mh.MaMonHoc equals gd.MaMonHoc into mhDangKy
                        select new
                        {
                            MonHoc = mh,
                            SoLuongSinhVien = mhDangKy.Count()
                        };

            foreach (var result in query)
            {
                Console.WriteLine("Môn học: " + result.MonHoc.TenMonHoc + " | Số lượng sinh viên: " + result.SoLuongSinhVien);
            }
        }
        public void ThongKeSoLuongSinhVien()
        {
            int soLuongSinhVien = SinhViens.Count();
            Console.WriteLine("Số lượng SV: " + soLuongSinhVien);
        }

        public void ThongKeSoLuongMonHocCoSinhVien()
        {
            var query = from mh in MonHocs
                        join gd in GhiDanhs on mh.MaMonHoc equals gd.MaMonHoc
                        select mh;

            int soLuongMonHocCoSinhVien = query.Distinct().Count();
            Console.WriteLine("Số lượng môn học có sinh viên: " + soLuongMonHocCoSinhVien);
        }

        public void ThongKeSoLuongMonHocKhongCoSinhVien()
        {
            var monHocCoSinhVien = from mh in MonHocs
                                   join gd in GhiDanhs on mh.MaMonHoc equals gd.MaMonHoc
                                   select mh.MaMonHoc;

            var monHocKhongCoSinhVien = GhiDanhs.Where(mh => !monHocCoSinhVien.Contains(mh.MaMonHoc));
            int soLuongMonHocKhongCoSinhVien = monHocKhongCoSinhVien.Count();
            Console.WriteLine("Số lượng môn học không có sinh viên: " + soLuongMonHocKhongCoSinhVien);
        }
    }
}
