﻿// See https://aka.ms/new-console-template for more information
using NguyenXuanHau_NPL.Practice.T02_Problem3;
using System.Xml;
using System.Xml.Linq;

class Program
{
    static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode;
        Console.InputEncoding = System.Text.Encoding.Unicode;
        Validation validation = new Validation();
        Console.WriteLine("-----TEST CASES---------");
        Student student = new Student(1, "Peter", DateTime.Now, 10, 9.5M, 10);
        student.Graduate();
        Console.WriteLine(String.Format("{0, -5}{1, -20}{2, -12}{3, -10}{4, -15}{5, -10}{6, -10}{7, -20}", "Id", "Name", "StartDate", "SqlMark", "CsharpMark", "DsaMark", "GPA", "Graduate Level"));
        string certificate = student.GetCertificate();
        Console.WriteLine(certificate);

        Console.WriteLine("------TEST YOUR OWN CASE--------");
        List<Student> students = new List<Student>();
        //     public int Id { get; set; }
        //public string Name { get; set; }
        //public DateTime StartDate { get; set; }
        //public decimal SqlMark { get; set; }
        //public decimal CsharpMark { get; set; }
        //public decimal DsaMark { get; set; }
        //public decimal GPA { get; set; }
        //public GraduateLevel GraduateLevel { get; set; }


        do
        {
            Console.WriteLine("Input student: ");
            int id;
            while (true)
            {
                id = validation.InputPostiveInteger("ID: ");
                if (students.Where(x => x.Id == id).Count() > 0)
                {
                    Console.WriteLine("ID existed!");
                }
                else
                {
                    break;
                }
            }
            string name = validation.InputString("Name: ", "^[A-Za-zÀ-ỹ\\s]+$");
            DateTime startDate = validation.InputDate("Date (dd/MM/yyyy): ");
            decimal sqlMark = validation.InputDecimalInRange("SQL Mark: ", 0, 10);
            decimal cSharpMark = validation.InputDecimalInRange("C# Mark: ", 0, 10);
            decimal dsaMark = validation.InputDecimalInRange("DSA Mark: ", 0, 10);
            students.Add(new Student(id, name, startDate, sqlMark, cSharpMark, dsaMark));
        } while (validation.InputYesNo("Do you want to input more? (Y/y or N/n): "));
        Console.WriteLine(String.Format("{0, -5}{1, -20}{2, -12}{3, -10}{4, -15}{5, -10}{6, -10}{7, -20}", "Id", "Name", "StartDate", "SqlMark", "CsharpMark", "DsaMark", "GPA", "Graduate Level"));
        foreach (Student s in students)
        {
            s.Graduate();
            certificate = s.GetCertificate();
            Console.WriteLine(certificate);
        }


    }
}