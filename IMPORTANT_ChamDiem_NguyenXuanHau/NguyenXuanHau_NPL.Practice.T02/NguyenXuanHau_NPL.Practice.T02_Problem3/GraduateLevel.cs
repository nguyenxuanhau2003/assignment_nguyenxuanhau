﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NguyenXuanHau_NPL.Practice.T02_Problem3
{
    internal enum GraduateLevel
    {
        Excellent,
        VeryGood,
        Good,
        AverageGood,
        Failed
    }
}
