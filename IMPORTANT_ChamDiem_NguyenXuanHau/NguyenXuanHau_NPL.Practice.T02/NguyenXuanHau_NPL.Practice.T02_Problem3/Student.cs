﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NguyenXuanHau_NPL.Practice.T02_Problem3
{
    internal class Student : IGraduate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public decimal SqlMark { get; set; }
        public decimal CsharpMark { get; set; }
        public decimal DsaMark { get; set; }
        public decimal GPA { get; set; }
        public GraduateLevel GraduateLevel { get; set; }

        public Student(int id, string name, DateTime startDate, decimal sqlMark, decimal csharpMark, decimal dsaMark)
        {
            Id = id;
            Name = name;
            StartDate = startDate;
            SqlMark = sqlMark;
            CsharpMark = csharpMark;
            DsaMark = dsaMark;
        }
        public void Graduate()
        {
            // tính GPA
            GPA = (SqlMark + CsharpMark + DsaMark) / 3;

            // Đặt GraduateLevel
            if (GPA >= 9)
            {
                GraduateLevel = GraduateLevel.Excellent;
            }
            else if (GPA >= 8)
            {
                GraduateLevel = GraduateLevel.VeryGood;
            }
            else if (GPA >= 7)
            {
                GraduateLevel = GraduateLevel.Good;
            }
            else if (GPA >= 5)
            {
                GraduateLevel = GraduateLevel.AverageGood;
            }
            else
            {
                GraduateLevel = GraduateLevel.Failed;
            }
        }
        public string GetCertificate()
        {
            return String.Format("{0, -5}{1, -20}{2, -12}{3, -10}{4, -15}{5, -10}{6, -10}{7, -20}", Id, Name, StartDate.ToString("dd/MM/yyyy"), SqlMark, CsharpMark, DsaMark, GPA.ToString("##.##"), GraduateLevel);
        }
    }
}
