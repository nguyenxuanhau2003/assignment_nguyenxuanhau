﻿
internal class Program
{
    static public int InputIntegerInRange(string message, int min, int max)
    {
        string input;
        int result;
        while (true)
        {
            try
            {
                Console.Write(message);
                input = Console.ReadLine();
                result = Convert.ToInt32(input);
                if (result < min || result > max)
                {
                    Console.WriteLine("Please input integer in range [" + min + ", " + max + "]!");
                    continue;
                }
                else
                {
                    break;
                }
            }
            catch (Exception ex)
            {
                Console.Write("Please input an integer number!");
            }
        }
        return result;
    }
    static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode;
        Console.InputEncoding = System.Text.Encoding.Unicode;
        Console.WriteLine("--------------TEST CASE 1--------------");
        int[] inputArray1 = { 1, 2, 5, -4, 3 };
        int subLength1 = 3;
        Console.WriteLine("Array: " + string.Join(", ", inputArray1));
        Console.WriteLine("Subarray length: " + subLength1);
        int maxSum1 = FindMaxSubArray(inputArray1, subLength1);
        Console.WriteLine("Maximum Subarray Sum: " + maxSum1);

        Console.WriteLine("--------------TEST CASE 2--------------");
        int[] inputArray2 = { -2, 1, -3, 4, -1, 2, 1, -5, 4 };
        int subLength2 = 4;
        Console.WriteLine("\nArray: " + string.Join(", ", inputArray2));
        Console.WriteLine("Subarray length: " + subLength2);
        int maxSum2 = FindMaxSubArray(inputArray2, subLength2);
        Console.WriteLine("Maximum Subarray Sum: " + maxSum2);

        Console.WriteLine("--------------TEST YOUR OWN CASE--------------");
        int[] inputArray;
        // Nhập mảng (có validate)
        while (true)
        {
            try
            {
                Console.WriteLine("Nhập các số nguyên của mảng, cách nhau bằng dấu phẩy (1,2,3,4):");
                string inputArrayString = Console.ReadLine();

                // Chuyển chuỗi đầu vào thành mảng các số nguyên
                inputArray = Array.ConvertAll(inputArrayString.Split(','), int.Parse);
                break;
            }
            catch(Exception ex)
            {
                Console.WriteLine("Vui lòng nhập lại do sai cú pháp!");
            }
        }

        // Nhập độ dài của dãy con (có validate)
        int subLength = InputIntegerInRange("Nhạp độ dài dãy con: ", 1, inputArray.Length);
        
        int maxSum = FindMaxSubArray(inputArray, subLength);
        Console.WriteLine("Tổng của dãy con liên tiếp lớn nhất: " + maxSum);
    }

    static public int FindMaxSubArray(int[] inputArray, int subLength)
    {
        int maxSum = int.MinValue; // Khởi tạo maxSum với giá trị thấp nhất có thể.
        int currentSum = 0; // Khởi tạo currentSum với giá trị 0.

        if (subLength <= 0 || subLength > inputArray.Length)
        {
            // Độ dài con mảng không hợp lệ.
            return maxSum;
        }

        for (int i = 0; i < inputArray.Length; i++)
        {
            // Cộng thêm giá trị của phần tử hiện tại
            currentSum += inputArray[i];
            // Nếu như đã đến hoặc quá phần tử thứ subLength - 1
            if (i >= subLength - 1)
            {
                if (currentSum > maxSum)
                {
                    maxSum = currentSum; // Cập nhật maxSum nếu tìm thấy tổng lớn hơn.
                }
                currentSum -= inputArray[i - subLength + 1]; // Loại bỏ phần tử đầu tiên của dãy con hiện tại.
            }
        }

        return maxSum; // Trả về tổng lớn nhất tìm thấy.
    }
}