﻿internal class Program
{
    static public string GetArticleSummary(string content, int maxLength)
    {
        if (content.Length <= maxLength)
        {
            // Nếu nội dung đã ngắn hơn hoặc bằng độ dài tối đa, không cần cắt.
            return content;
        }
        else
        {
            // Tìm chỉ số của khoảng trắng cuối cùng trong độ dài tối đa
            int lastSpaceIndex = content.LastIndexOf(' ', maxLength);

            if (lastSpaceIndex == -1)
            {
                // Nếu không tìm thấy khoảng trắng, đơn giản cắt ở vị trí độ dài tối đa và thêm "…" để chỉ ra cắt bớt.
                return content.Substring(0, maxLength) + "...";
            }
            else
            {
                // Cắt nội dung tại khoảng trắng cuối cùng trong độ dài tối đa
                return content.Substring(0, lastSpaceIndex) + "...";
            }
        }
    }

    
    private static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode;
        Console.InputEncoding = System.Text.Encoding.Unicode;
        Console.WriteLine("--------TEST CASE 1--------");
        string content1 = "One of the world's biggest festivals hit the streets of London";
        int maxLength1 = 50;
        Console.WriteLine("Content: " + content1);
        Console.WriteLine("Max length: " + maxLength1);
        string summary1 = GetArticleSummary(content1, maxLength1);
        Console.WriteLine("Summary: " + summary1);

        Console.WriteLine();
        Console.WriteLine("--------TEST CASE 2--------");
        string content2 = "This is a sample article for testing the summary extraction";
        int maxLength2 = 30;
        Console.WriteLine("Content: " + content2);
        Console.WriteLine("Max length: " + maxLength2);
        string summary2 = GetArticleSummary(content2, maxLength2);
        Console.WriteLine("Summary: " + summary2);

        Console.WriteLine();
        Console.WriteLine("--------TEST YOUR OWN CASE--------");
        Console.WriteLine("Input content: ");
        string content = Console.ReadLine();
        string input;
        int maxLength;
        while (true)
        {
            try
            {
                Console.Write("Input max length: ");
                input = Console.ReadLine();
                maxLength = Convert.ToInt32(input);
                if (maxLength <= 0)
                {
                    Console.WriteLine("Input maxlength > 0");
                    continue;
                }
                else
                {
                    break;
                }
            }
            catch (Exception ex)
            {
                Console.Write("Please input number!");
            }
        }

        Console.WriteLine(GetArticleSummary(content, maxLength));

    }
}