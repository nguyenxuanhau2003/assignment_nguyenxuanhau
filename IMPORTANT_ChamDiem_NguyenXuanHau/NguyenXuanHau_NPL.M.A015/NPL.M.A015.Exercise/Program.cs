﻿
using NPL.M.A015.Exercise;
using System.Collections.Generic;
Console.OutputEncoding = System.Text.Encoding.Unicode;
Console.InputEncoding = System.Text.Encoding.Unicode;
Manager manager = new Manager();
Validation validation = new Validation();
while (true)
{
    Console.WriteLine("Employee Management System Menu:");
    Console.WriteLine("1. Get Departments by Number of Employees");
    Console.WriteLine("2. Get Employees Working");
    Console.WriteLine("3. Get Employees by Language");
    Console.WriteLine("4. Get Languages by Employee ID");
    Console.WriteLine("5. Get Senior Employees");
    Console.WriteLine("6. Get Employees with Paging and Sorting");
    Console.WriteLine("7. Get Departments with Employees");
    Console.WriteLine("8. Input Department");
    Console.WriteLine("9. Input Employee");
    Console.WriteLine("10. Input programing Language");
    Console.WriteLine("11. Input Employee Language");
    Console.WriteLine("12. Exit");

    int choice = validation.InputPostiveInteger("Select an option: ");

    switch (choice)
    {
        case 1:
            // Department có số nhân viên lớn hơn hoặc bằng numberOfEmployees
            int numberOfEmployees = validation.InputPostiveInteger("Input minium number of employee: ");
            if (manager.GetDepartments(numberOfEmployees).Count() > 0)
            {
                Console.WriteLine("The departments have more or equal than " + numberOfEmployees + " are: ");
                foreach (Department dp in manager.GetDepartments(numberOfEmployees))
                {
                    Console.WriteLine(dp.DepartmentId + "-" + dp.DepartmentName);
                }
            }
            else
            {
                Console.WriteLine("There is no department has the number of employee greater than or equal " + numberOfEmployees + "!");
            }
            break;

        case 2:
            if (manager.GetEmployeesWorking().Count() > 0)
            {
                Console.WriteLine("Working employees: ");
                foreach (Employee e in manager.GetEmployeesWorking())
                {
                    Console.WriteLine(e.EmployeeId + ". " + e.EmployeeName);
                }

            }
            else
            {
                Console.WriteLine("There is no working employee");
            }
            break;
        case 3:
            Console.Write("Input programming language: ");
            string programmingLanguage = Console.ReadLine();
            if (manager.GetEmployees(programmingLanguage).Count() > 0)
            {
                Console.WriteLine("Employees can use " + programmingLanguage + " are:");
                foreach (Employee e in manager.GetEmployees(programmingLanguage))
                {
                    Console.WriteLine(e.DepartmentId + "-" + e.EmployeeName);
                }
            }
            else
            {
                Console.WriteLine("No Employee can use language " + programmingLanguage + "!");
            }
            break;

        case 4:
           
                Console.Write("Input employeeId: ");
                int employeeId = int.Parse(Console.ReadLine());
            if (manager.GetLanguages(employeeId).Count() > 0)
            {
                Console.WriteLine("The programming language that employee can use:");
                foreach (ProgramingLanguage prlg in manager.GetLanguages(employeeId))
                {
                    Console.WriteLine(prlg.LanguageName);
                }
            }
            else
            {
                Console.WriteLine("No programming language that employee can use!");
            }
            break;

        case 5:
            if (manager.GetSeniorEmployee().Count() > 0)
            {
                Console.WriteLine("Senior employees: ");
                foreach (Employee e in manager.GetSeniorEmployee())
                {
                    Console.WriteLine(e.EmployeeName);
                }
            }
            else
            {
                Console.WriteLine("No senior Employee");
            }
            break;

        case 6:
            int pageIndex = validation.InputPostiveInteger("Page Index: ");
            int pageSize = validation.InputPostiveInteger("Page Size: ");
            string employeeName = validation.InputString("Employee Name: ", "^[A-Za-zÀ-ỹ\\s]+$");
            Console.WriteLine("ASC | DESC: ");
            string order = Console.ReadLine();


            if (manager.GetEmployeePaging(pageIndex, pageSize, employeeName, order).Count() == 0)
            {
                Console.WriteLine("Do not have suitable students!");
            }
            else
            {
                Console.WriteLine("Employees in page " +pageIndex+ ", contains \"" + employeeName + "\" are: ");
                foreach (Employee e in manager.GetEmployeePaging(pageIndex, pageSize, employeeName, order))
                {
                    Console.WriteLine(e.EmployeeId + "-" + e.EmployeeName);
                }
            }
            break;

        case 7:
            Dictionary<Department, List<Employee>> departmentEmployee = manager.GetDepartments();
            foreach(var dep in departmentEmployee)
            {
                if (dep.Value.Count > 0)
                {
                    Console.WriteLine("Employee in " + dep.Key.DepartmentName + ":");

                    foreach (var emp in departmentEmployee[dep.Key])
                    {
                        Console.WriteLine("-" + emp.EmployeeName);
                    }
                }
                else
                {
                    Console.WriteLine("No Employee in " + dep.Key.DepartmentName + ".");
                }
            }
            break;
        case 8:
            manager.InputDepartment();
            break;
        case 9:
            manager.InputEmployee();
            break;
        case 10:
            manager.InputProgramingLanguage();
            break;
        case 11:
            manager.InputLanguageEmployee();
            break;
        case 12:
            Environment.Exit(0);
            break;

        default:
            Console.WriteLine("Invalid option. Please choose a valid option.");
            break;
    }
}
