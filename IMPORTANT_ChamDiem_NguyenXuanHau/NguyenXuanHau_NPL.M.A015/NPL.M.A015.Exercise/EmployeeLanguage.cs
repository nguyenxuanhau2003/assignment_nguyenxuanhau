﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class EmployeeLanguage
    {
        public int EmployeeId {  get; set; }
        public string LanguageName {  get; set; }

        public EmployeeLanguage()
        {
        }

        public EmployeeLanguage(int employeeId, string languageName)
        {
            EmployeeId = employeeId;
            LanguageName = languageName;
        }
    }
}
