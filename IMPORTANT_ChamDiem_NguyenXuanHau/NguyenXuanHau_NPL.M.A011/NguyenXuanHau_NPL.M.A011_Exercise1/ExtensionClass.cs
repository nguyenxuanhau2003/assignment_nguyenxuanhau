﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NguyenXuanHau_NPL.M.A011_Exercise1
{
    public static class ExtensionClass
    {
        // Phương thức mở rộng này đếm số lượng phần tử có kiểu dữ liệu int trong ArrayList.
        //Solution 1:
        public static int CountInt(this ArrayList array)
        {
            return array.OfType<int>().Count();
        }
        //Solution 2:
        //public static int CountInt(this ArrayList array)
        //{
        //    int count = 0;
        //    foreach (object obj in array)
        //    {
        //        if (obj is int)
        //        {
        //            count++;
        //        }
        //    }
        //    return count;
        //}

        //-------------------------------------------------
        //Phương thức mở rộng này đếm số lượng phần tử có kiểu dữ liệu là dataType trong ArrayList.
        //Solution 1:
        public static int CountOf(this ArrayList array, Type dataType)
        {
            return array.Cast<object>().Count(item => dataType.IsInstanceOfType(item));
        }
        //Solution 2:
        //public static int CountOf(this ArrayList array, Type dataType)
        //{
        //    int count = 0;
        //    foreach (object obj in array)
        //    {
        //        if (obj.GetType() == dataType)
        //        {
        //            count++;
        //        }
        //    }
        //    return count;
        //}
        //-------------------------------------------------
        // Phương thức mở rộng này đếm số lượng phần tử có kiểu dữ liệu là T trong ArrayList.
        //Solution 1:
        public static int CountOf<T>(this ArrayList array)
        {
            return array.OfType<T>().Count();
        }
        //Solution 2
        //public static int CountOf<T>(this ArrayList array)
        //{
        //    int count = 0;
        //    foreach (object obj in array)
        //    {
        //        if (obj.GetType() == typeof(T))
        //        {
        //            count++;
        //        }
        //    }
        //    return count;
        //}

        // Phương thức mở rộng này trả về giá trị lớn nhất của kiểu T nếu T là kiểu số, ngược lại ném một ngoại lệ.
        public static T MaxOf<T>(this ArrayList array) where T : IComparable<T>
        {
            if (IsNumericType(typeof(T)))
            {
                T max;
                bool check = false;
                for (int i = 0; i < array.Count; i++)
                {
                    if (array[i].GetType() == typeof(T))
                    {
                        check = true;
                        max = (T)array[i];
                        foreach (var item in array)
                        {
                            if (item is T tItem)
                            {
                                if (tItem.CompareTo(max) > 0)
                                {
                                    max = tItem;
                                }
                            }
                        }
                        return max;
                    }
                }
            }
            else
            {
                throw new InvalidOperationException("T must be a numeric data type.");
            }
            return (T)array[0];
        }

        private static bool IsNumericType(Type type)
        {
            return type.IsPrimitive && type != typeof(bool);
        }

    }
}
