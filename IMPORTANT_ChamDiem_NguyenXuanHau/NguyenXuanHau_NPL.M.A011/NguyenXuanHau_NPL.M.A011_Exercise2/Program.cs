﻿using NguyenXuanHau_NPL.M.A011_Exercise2;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;

int[] arr = new int[] { 1, 2, 3, 3, 5, 6, 5, 2 };
string[] arr2 = new string[] { "Hung", "Vu", "Van", "Hung", "Quang", "Huy", "Vu" };
Console.WriteLine("Mảng số sau khi lọc trùng: ");
Console.WriteLine(string.Join(" ", arr.RemoveDuplicate()));


Console.WriteLine("Mảng sau khi lọc trùng: ");
Console.WriteLine(string.Join(" ", arr2.RemoveDuplicate<string>()));