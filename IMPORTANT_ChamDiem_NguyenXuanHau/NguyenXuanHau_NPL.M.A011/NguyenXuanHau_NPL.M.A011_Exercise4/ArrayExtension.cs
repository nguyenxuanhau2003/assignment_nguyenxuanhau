﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NguyenXuanHau_NPL.M.A011_Exercise4
{
    static internal class ArrayExtension
    {
        public static T[] RemoveDuplicate<T>(this T[] array)
        {
            List<T> intList = new List<T>();


            foreach (T i in array)
            {
                if (intList.Contains(i) == false)
                {
                    intList.Add(i);
                }
            }

            return intList.ToArray();
        }

        public static T SecondLargest<T>(this T[] array) where T : IComparable<T>
        {
            if (array.Length < 2)
            {
                throw new InvalidOperationException("Mảng phải có ít nhất 2 phần tử.");
            }

            return array.OrderByDescending(x => x).ElementAt(1);
        }

        public static T OrderLargest<T>(this T[] array, int order) where T : IComparable<T>
        {
            if (order < 1 || order > array.Length)
            {
                throw new ArgumentException("Thứ tự không hợp lệ.");
            }

            return array.OrderByDescending(x => x).ElementAt(order - 1);
        }
    }
}
