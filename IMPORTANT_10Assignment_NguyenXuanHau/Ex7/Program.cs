﻿using Ex7;

Console.OutputEncoding = System.Text.Encoding.Unicode;
Console.InputEncoding = System.Text.Encoding.Unicode;
int luaChon;
Validation validate = new Validation();
Service service = new Service();
service.Init();
do
{
    Console.WriteLine("===========MENU==========");
    Console.WriteLine("Chương trình quản lý sinh viên");
    Console.WriteLine("1. Nhập danh sách đối tượng");
    Console.WriteLine("2. Xuất danh sách đối tượng");
    Console.WriteLine("3. Xuất các đối tượng có bao gồm thêm thông tin năm sinh");
    Console.WriteLine("4. Xóa đối tượng theo ID và thông báo không tìm thấy khi nhập mã không tồn tại");
    Console.WriteLine("0. Thoát.");
    luaChon = validate.InputInt("Mời bạn nhập lựa chọn: ", 0, 4);

    switch (luaChon)
    {
        case 1:
            service.ThemSinhVien();
            break;
        case 2:
            service.HienThiSinhVien();
            break;
        case 3:
            service.HienThiSinhVienVoiNam();
            break;
        case 4:
            service.DeleteById();
            break;
        case 0:
            Console.WriteLine("Kết thúc chương trình!");
            break;
        default:
            Console.WriteLine("Không có lựa chọn!");
            break;

    }

}
while (luaChon > 0 && luaChon <= 4);

