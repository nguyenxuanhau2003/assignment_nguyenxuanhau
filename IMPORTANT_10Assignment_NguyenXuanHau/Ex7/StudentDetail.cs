﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex7
{
    internal class StudentDetail : Student
    {
        public StudentDetail() { }

        public StudentDetail(int id, string ten, int tuoi, string nganh) : base(id, ten, tuoi, nganh)
        {
        }

        public override void InThongTin()
        {
            Console.WriteLine(string.Format("{0, -15} {1, -20} {2, -10} {3, -10} {4, -10}", Id, Ten, Tuoi, Nganh, DateTime.Now.Year - this.Tuoi));
        }
    }

}
