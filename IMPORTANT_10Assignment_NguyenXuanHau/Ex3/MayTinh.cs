﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex3
{
    internal class MayTinh
    {
        public string ID { get; set; }
        public string Ten { get; set; }
        public float TrongLuong { get; set; }

        public MayTinh() { }
        public MayTinh(string iD, string ten, float trongLuong)
        {
            ID = iD;
            Ten = ten;
            TrongLuong = trongLuong;
        }

        public void InThongTin()
        {
            Console.WriteLine(string.Format("{0, -5}{1, -15}{2, -5}", ID, Ten, TrongLuong));
        }
    }

}
