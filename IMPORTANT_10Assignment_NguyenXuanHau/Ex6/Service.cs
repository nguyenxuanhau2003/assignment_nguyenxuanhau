﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex6
{
    internal class Service
    {
        Validation validation = new Validation();
        List<HocSinh> HocSinhList = new List<HocSinh>();
        List<HocSinhNamSinh> HocSinhNamSinhList = new List<HocSinhNamSinh>();

        object[] mang = new object[10];
        public string NhapMaHocSinh()
        {
            string maHocSinh;
            bool check = true;
            while (true)
            {
                maHocSinh = validation.InputString("Nhập Mã Học Sinh: ", "^[A-Za-z0-9]+$");
                foreach (HocSinh hocsinh in HocSinhList)
                {
                    check = true;
                    if (maHocSinh.Equals(hocsinh.MaHs))
                    {
                        check = false;
                        Console.WriteLine("Mã học sinh đã tồn tại");
                        break;
                    }
                }
                if (check)
                {
                    return maHocSinh;
                }
            }
        }

        public void NhapDanhSachDoiTuong()
        {
            do
            {
                string maHs = NhapMaHocSinh();
                string ten = validation.InputString("Nhập tên: ", "^[a-zA-ZÀ-ỹ\\s]+$");
                int tuoi = validation.InputPostiveInteger("Nhập tuổi: ");
                HocSinhList.Add(new HocSinh(maHs, ten, tuoi));
            }
            while (validation.InputYesNo("Có muốn nhập tiếp không? (Y/y N/n): "));
        }

        public void XuatDoiTuong()
        {
            Console.WriteLine(string.Format("{0, -10}{1, -30}{2, -5}", "Mã HS", "Họ và tên", "Tuổi"));
            foreach (HocSinh hocSinh in HocSinhList)
            {
                hocSinh.InThongTin();
            }
        }

        public void XuatDoiTuongThemNamSinh()
        {
            foreach (HocSinh hocsinh in HocSinhList)
            {
                HocSinhNamSinhList.Add(new HocSinhNamSinh(hocsinh.MaHs, hocsinh.Ten, hocsinh.Tuoi));
            }
            Console.WriteLine(string.Format("{0, -10}{1, -30}{2, -5}{3, -10}", "Mã HS", "Họ và tên", "Tuổi", "Năm sinh"));
            foreach (HocSinhNamSinh hocSinhNamSinh in HocSinhNamSinhList)
            {
                hocSinhNamSinh.InThongTin();
            }
        }

        public void XoaHocSinhTheoMa()
        {
            string maHocSinh;
            maHocSinh = validation.InputString("Nhập mã học sinh cần xoá: ", "^[A-Za-z0-9]+$");
            bool check = false;
            foreach (HocSinh hocsinh in HocSinhList)
            {
                if (hocsinh.MaHs.Equals(maHocSinh))
                {
                    HocSinhList.Remove(hocsinh);
                    Console.WriteLine("Đã xoá Laptop với mã = " + maHocSinh);
                    check = true;
                    break;
                }
            }
            if (check == false)
            {
                Console.WriteLine("Không tìm thấy mã laptop nào!");
            }

        }

        public void Init()
        {
            HocSinhList.Add(new HocSinh("1", "Nguyễn Xuân Hậu", 13));
            HocSinhList.Add(new HocSinh("2", "Nguyễn Tuấn Ninh", 18));
            HocSinhList.Add(new HocSinh("3", "Đinh Đức Minh", 19));
        }
    }
}
