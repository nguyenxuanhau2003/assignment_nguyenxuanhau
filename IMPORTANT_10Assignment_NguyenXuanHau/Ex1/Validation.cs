﻿// See https://aka.ms/new-console-template for more information
using Ex1;
using System.Text.RegularExpressions;

internal class Validation
{
    public string InputString(string message, string pattern)
    {
        string input;
        while (true)
        {
            Console.WriteLine(message);
            input = Console.ReadLine();
            if (!Regex.IsMatch(input, pattern) || input.Equals(""))
            {
                Console.WriteLine("Nhập không hợp lệ");
                continue;
            }
            return input;
        }

    }

    public DateTime InputDate(string message)
    {
        while (true)
        {
            Console.WriteLine(message);
            string intput = Console.ReadLine();
            try
            {
                DateTime date = DateTime.ParseExact(intput, "dd/MM/yyyy", null);
                return date;
            }
            catch
            {
                Console.WriteLine("Ngày không đúng format: ");
            }
        }

    }

    public int InputInt(string mess, int min, int max)
    {
        Console.WriteLine(mess);
        while (true)
        {
            try
            {
                int number = int.Parse(Console.ReadLine());
                //check range of number
                if (number < min || number > max)
                {
                    Console.WriteLine("Nhập số giữa " + min + ", " + max + ": ");
                    continue;
                }
                return number;
            }
            catch
            {
                Console.WriteLine("Nhập số nguyên!");
            }
        }
    }

    public double InputDouble(string mess, double min, double max)
    {
        Console.WriteLine(mess);
        while (true)
        {
            try
            {
                double number = double.Parse(Console.ReadLine());
                if (number < min || number > max)
                {
                    Console.WriteLine("Please input between " + min + ", " + max + ": ");
                    continue;
                }
                return number;
            }
            catch
            {
                Console.WriteLine("Please input an double number: ");
            }
        }
    }


    public bool CheckInputYN()
    {
        while (true)
        {
            string result = Console.ReadLine();
            if (result.Equals("Y") || result.Equals("y"))
            {
                return true;
            }
            else if (result.Equals("N") || result.Equals("n"))
            {
                return false;
            }
            Console.WriteLine("Please input y/Y or n/N.");
            Console.WriteLine("Enter again: ");
        }
    }

    public bool CheckIdExist(List<SinhVien> sinhViens, string id)
    {
        foreach (SinhVien sinhvien in sinhViens)
        {
            if (sinhvien.MaSV.Equals(id))
            {
                Console.WriteLine("Mã sinh viên đã tồn tại");
                return false;
            }
        }
        return true;
    }
}
