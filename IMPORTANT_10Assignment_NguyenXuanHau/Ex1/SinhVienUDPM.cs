﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex1
{
    internal class SinhVienUDPM : SinhVien
    {
        public double DiemJava { get; set; }
        public double DiemCsharp { get; set; }

        public SinhVienUDPM() { }
        public SinhVienUDPM(double diemJava, double diemCsharp)
        {
            DiemJava = diemJava;
            DiemCsharp = diemCsharp;
        }

        public SinhVienUDPM(string maSV, string ten, int namSinh, double diemJava, double diemCsharp) : base(maSV, ten, namSinh)
        {
            DiemJava = diemJava;
            DiemCsharp = diemCsharp;
        }

        public override string InThongTin()
        {
            return base.InThongTin() + string.Format("{0, -10} {1, -5}", DiemJava, DiemCsharp);
        }
    }

}
