﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex5
{
    internal class Covid
    {
        public string MaCovid { get; set; }
        public string Ten { get; set; }
        public int NamPhatHien { get; set; }

        public Covid(string maCovid, string ten, int namPhatHien)
        {
            MaCovid = maCovid;
            Ten = ten;
            NamPhatHien = namPhatHien;
        }

        public void InThongTin()
        {
            Console.WriteLine(string.Format("{0, -15} {1, -20} {2, -15}", MaCovid, Ten, NamPhatHien));
        }
    }

}
