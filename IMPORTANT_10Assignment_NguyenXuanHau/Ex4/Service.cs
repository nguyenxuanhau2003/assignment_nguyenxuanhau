﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex4
{
    internal class Service
    {
        Validation validation = new Validation();
        List<LapTop> LapTopList = new List<LapTop>();
        public int NhapIdTuDong()
        {
            if (LapTopList.Count == 0) return 1;
            return LapTopList[LapTopList.Count - 1].Id + 1;
        }

        public string NhapMaLapTop()
        {
            string maLapTop;
            bool check = true;
            while (true)
            {
                maLapTop = validation.InputString("Nhập Mã Laptop: ", "^[A-Za-z0-9]+$");
                foreach (LapTop laptop in LapTopList)
                {
                    check = true;
                    if (maLapTop.Equals(laptop.MaLapTop))
                    {
                        check = false;
                        Console.WriteLine("Mã laptop đã tồn tại");
                        break;
                    }
                }
                if (check)
                {
                    return maLapTop;
                }
            }
        }

        public void NhapDanhSachDoiTuong()
        {
            do
            {
                int id = NhapIdTuDong();
                string maLapTop = NhapMaLapTop();
                double kichThuocManHinh = validation.InputPositiveDouble("Nhập kích thước màn hình: ");
                LapTopList.Add(new LapTop(id, maLapTop, kichThuocManHinh));
            }
            while (validation.InputYesNo("Có muốn nhập tiếp không? (Y/y N/n): "));
        }

        public void XuatDoiTuong()
        {
            Console.WriteLine(string.Format("{0, -5}{1, -10}{2, -20}", "ID", "Mã Laptop", "Kích thước màn hình"));
            foreach (LapTop laptop in LapTopList)
            {
                laptop.InThongTin();
            }
        }

        public void XoaLapTopTheoMa()
        {
            string maLaptop;
            maLaptop = validation.InputString("Nhập mã laptop cần xoá: ", "^[A-Za-z0-9]+$");
            bool check = false;
            foreach (LapTop laptop in LapTopList)
            {
                if (laptop.MaLapTop.Equals(maLaptop))
                {
                    LapTopList.Remove(laptop);
                    Console.WriteLine("Đã xoá Laptop với mã = " + maLaptop);
                    check = true;
                    break;
                }
            }
            if (check == false)
            {
                Console.WriteLine("Không tìm thấy mã laptop nào!");
            }

        }

        public void XuatLapTopTheoKhoangManHinh()
        {
            double min;
            double max;
            while (true)
            {
                min = validation.InputNoNegativeDouble("Nhập màn hình min: ");
                max = validation.InputNoNegativeDouble("Nhập màn hình max: ");
                if (min > max)
                {
                    Console.WriteLine("Nhạp min <= max!");
                }
                else
                {
                    break;
                }
            }
            bool check = false;
            Console.WriteLine(string.Format("{0, -5}{1, -10}{2, -20}", "ID", "Mã Laptop", "Kích thước màn hình"));
            foreach (LapTop lapTop in LapTopList)
            {
                if (lapTop.KichThuocMH >= min && lapTop.KichThuocMH <= max)
                {
                    lapTop.InThongTin();
                    check = true;
                }
            }
            if (check == false)
            {
                Console.WriteLine("Không có laptop nào thoả mãn!");
            }
        }

        public void Init()
        {
            LapTopList.Add(new LapTop(1, "ABC", 23));
            LapTopList.Add(new LapTop(2, "DEF", 21));
            LapTopList.Add(new LapTop(3, "XYZ", 22));
        }
    }
}
