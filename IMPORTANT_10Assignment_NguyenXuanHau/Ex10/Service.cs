﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex10
{
    internal class Service
    {
        Validation validation = new Validation();
        List<NganhHoc> DanhSachNganhHoc = new List<NganhHoc>();
        internal void Init()
        {
            DanhSachNganhHoc.Add(new NganhHoc(1, "Kỹ thuật phần mềm", 9));
            DanhSachNganhHoc.Add(new NganhHoc(2, "An Toàn Thông Tin", 6));
            DanhSachNganhHoc.Add(new NganhHoc(3, "Trí Tuệ Nhanh Tạo", 10));
        }

        public int NhapIdDeThem()
        {
            int id;
            bool check = true;
            while (true)
            {
                id = validation.InputPostiveInteger("Nhap ID: ");
                foreach (NganhHoc nganhHoc in DanhSachNganhHoc)
                {
                    check = true;
                    if (id == nganhHoc.Id)
                    {
                        check = false;
                        Console.WriteLine("ID đã tồn tại");
                        break;
                    }
                }
                if (check)
                {
                    return id;
                }
            }
        }

        public void NhapDanhSachDoiTuong()
        {
            do
            {
                int id = NhapIdDeThem();
                string ten = validation.InputString("Nhập tên: ", "^[a-zA-ZÀ-ỹ\\s]+$");
                int soKyHoc = validation.InputPostiveInteger("Nhập số kỳ học: ");
                DanhSachNganhHoc.Add(new NganhHoc(id, ten, soKyHoc));
            }
            while (validation.InputYesNo("Có muốn nhập tiếp không? (Y/y N/n): "));
        }
        internal void XuatDoiTuong()
        {
            Console.WriteLine(string.Format("{0, -5}{1, -20}{2, -10}", "ID", "Tên ngành học", "Số kì học"));
            foreach (NganhHoc nganhHoc in DanhSachNganhHoc)
            {
                nganhHoc.InThongTin();
            }
        }
        internal void XuatDanhSachDoiTuongHocKyHon6()
        {
            Console.WriteLine(string.Format("{0, -5}{1, -20}{2, -10}", "ID", "Tên ngành học", "Số kì học"));
            foreach (NganhHoc nganhHoc in DanhSachNganhHoc)
            {
                if (nganhHoc.SoKyHoc > 6)
                {
                    nganhHoc.InThongTin();
                }
            }
        }
        internal void XoaHocKyTheoId()
        {
            int id;
            id = validation.InputPostiveInteger("Nhập ID cần xoá: ");
            bool check = false;
            foreach (NganhHoc nganhHoc in DanhSachNganhHoc)
            {
                if (nganhHoc.Id == id)
                {
                    DanhSachNganhHoc.Remove(nganhHoc);
                    Console.WriteLine("Đã xoá ngành học với ID = " + id);
                    check = true;
                    break;
                }
            }
            if (check == false)
            {
                Console.WriteLine("Không tìm thấy ngành học nào!");
            }
        }
    }
}
