﻿using Ex10;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;


Validation validation = new Validation();
int choice;

Service service = new Service();
service.Init();
do
{
    Console.WriteLine("=========MENU =========");
    Console.WriteLine("1. Nhập danh sách đối tượng");
    Console.WriteLine("2. Xuất danh sách đối tượng");
    Console.WriteLine("3. Xuất các ngành học có kỳ lớn hơn 6");
    Console.WriteLine("4. Xoá đối tượng theo ID");
    Console.WriteLine("0. Thoát");
    choice = validation.InputIntegerInRange("Chọn chức năng: ", 0, 4);
    switch (choice)
    {
        case 1:
            service.NhapDanhSachDoiTuong();
            break;
        case 2:
            service.XuatDoiTuong();
            break;
        case 3:
            service.XuatDanhSachDoiTuongHocKyHon6();
            break;
        case 4:
            service.XoaHocKyTheoId();
            break;
        default:
            break;
    }
} while (choice > 0 && choice <= 4);
Console.ReadKey();
