﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex2
{
    internal class GiaoVienPoly : GiaoVien
    {
        public string NganhDay { get; set; }
        public override void InThongTin()
        {
            Console.WriteLine(string.Format("{0, -5}{1, -30}{2, -5}{3, -15}", this.Id, this.Ten, this.SoGioDay, this.NganhDay));
        }
    }
}
