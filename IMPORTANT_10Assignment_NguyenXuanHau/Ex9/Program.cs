﻿
using Ex9;

Console.OutputEncoding = System.Text.Encoding.Unicode;
Console.InputEncoding = System.Text.Encoding.Unicode;
int luaChon;
Validation validate = new Validation();
Service service = new Service();
service.Init();
do
{
    Console.WriteLine("===========MENU==========");
    Console.WriteLine("1. Nhập danh sách đối tượng");
    Console.WriteLine("2. Xuất danh sách đối tượng");
    Console.WriteLine("3. Xuất các xe máy của hãng HONDA");
    Console.WriteLine("4. Sắp xếp đối tượng theo ID giảm dần");
    Console.WriteLine("0. Thoát.");
    luaChon = validate.InputInt("Mời bạn nhập lựa chọn: ", 0, 4);

    switch (luaChon)
    {
        case 1:
            service.ThemXe();
            break;
        case 2:
            service.HienThiXe();
            break;
        case 3:
            service.HienThiHonda();
            break;
        case 4:
            service.SapXepTheoId();
            break;
        case 0:
            Console.WriteLine("Kết thúc chương trình!");
            break;
        default:
            Console.WriteLine("Không có lựa chọn!");
            break;

    }

}
while (luaChon > 0 && luaChon <= 4);


