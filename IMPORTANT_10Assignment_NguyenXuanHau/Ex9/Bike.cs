﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex9
{
    internal class Bike
    {
        public int Id { get; set; }
        public string Ten { get; set; }
        public string HSX { get; set; }

        public Bike() { }
        public Bike(int id, string ten, string hSX)
        {
            Id = id;
            Ten = ten;
            HSX = hSX;
        }

        public virtual void InThongTin()
        {
            Console.WriteLine(string.Format("{0, -15} {1, -20} {2, -15}", Id, Ten, HSX));
        }
    }


}
