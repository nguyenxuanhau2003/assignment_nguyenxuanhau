﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex9
{
    internal class Service
    {
        public List<Bike> listBikes = new List<Bike>();

        Validation validate = new Validation();
        public void ThemXe()
        {
            while (true)
            {
                int id;
                while (true)
                {
                    id = validate.InputInt("Nhập ID: ", 0, 10000);
                    if (validate.CheckIdExist(listBikes, id))
                    {
                        break;
                    }
                }
                string name = validate.InputString("Tên xe đạp: ", "[A-Za-z\\s]+");
                string HSX = validate.InputString("Hãng sản xuất: ", "[A-Za-z\\s]+");
                listBikes.Add(new Bike(id, name, HSX));

                Console.WriteLine("Bạn có muốn nhập tiếp không: ");
                if (!validate.CheckInputYN())
                {
                    return;
                }
            }
        }


        public void HienThiXe()
        {
            Console.WriteLine(string.Format("{0, -15} {1, -20} {2, -15}", "Mã", "Tên", "Hãng sản xuất"));
            foreach (Bike bike in listBikes)
            {
                bike.InThongTin();
            }
        }

        public void HienThiHonda()
        {
            Console.WriteLine(string.Format("{0, -15} {1, -20} {2, -15}", "Mã", "Tên", "Hãng sản xuất"));
            foreach (Bike bike in listBikes)
            {
                if (bike.HSX.ToUpper().Equals("HONDA"))
                {
                    bike.InThongTin();
                }
            }
        }

        public void SapXepTheoId()
        {
            for (int i = 0; i < listBikes.Count; i++)
            {
                for (int j = i + 1; j < listBikes.Count; ++j)
                {
                    if (listBikes[i].Id < listBikes[j].Id)
                    {
                        (listBikes[i], listBikes[j]) = (listBikes[j], listBikes[i]);
                    }
                }
            }
            Console.WriteLine("Sắp xếp thành công");
        }

        public void Init()
        {
            listBikes.Add(new Bike(1, "Hậu", "Honda"));
            listBikes.Add(new Bike(2, "Ninh", "Suzuki"));
            listBikes.Add(new Bike(3, "Minh", "Toyota"));
            listBikes.Add(new Bike(4, "Phúc", "Suzuki"));
            listBikes.Add(new Bike(5, "Đạt", "Honda"));
        }

    }

}
