﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex8
{
    internal class Service
    {
        List<Teacher> TeacherList = new List<Teacher>();
        Validation validation = new Validation();
        public int NhapIdDeThem()
        {
            int id;
            bool check = true;
            while (true)
            {
                id = validation.InputPostiveInteger("Nhap ID: ");
                foreach (Teacher teacher in TeacherList)
                {
                    check = true;
                    if (id == teacher.Id)
                    {
                        check = false;
                        Console.WriteLine("ID đã tồn tại");
                        break;
                    }
                }
                if (check)
                {
                    return id;
                }
            }
        }

        public void NhapDanhSachDoiTuong()
        {
            do
            {
                int id = NhapIdDeThem();
                string maGv = validation.InputString("Nhập mã GV: ", "^[a-zA-Z\\s]+$");
                string nganh = validation.InputString("Nhập ngành: ", "^[a-zA-ZÀ-ỹ\\s]+$");
                TeacherList.Add(new Teacher(id, maGv, nganh));
            }
            while (validation.InputYesNo("Có muốn nhập tiếp không? (Y/y N/n): "));
        }

        public void XuatDoiTuong()
        {
            Console.WriteLine(string.Format("{0, -5}{1, -10}{2, -20}", "ID", "MaGV", "Ngành"));
            foreach (Teacher teacher in TeacherList)
            {
                teacher.InThongTin();
            }
        }

        public void XuatCacTeacherUDPM()
        {
            Console.WriteLine(string.Format("{0, -5}{1, -10}{2, -20}", "ID", "MaGV", "Ngành"));
            foreach (Teacher teacher in TeacherList)
            {
                if (teacher.Nganh.ToLower().Equals("udpm"))
                {
                    teacher.InThongTin();
                }
            }
        }

        public void SapXepDoiTuongTheoNganh()
        {
            Console.WriteLine(string.Format("{0, -5}{1, -10}{2, -20}", "ID", "MaGV", "Ngành"));
            List<Teacher> DanhSachSapXep = new List<Teacher>();
            foreach (Teacher teacher in TeacherList)
            {
                DanhSachSapXep.Add(teacher);
            }
            Teacher temp;
            for (int i = 0; i < TeacherList.Count - 1; i++)
            {
                for (int j = i + 1; j < TeacherList.Count; j++)
                {
                    if (DanhSachSapXep[i].Nganh.CompareTo(DanhSachSapXep[j].Nganh) > 0)
                    {
                        temp = DanhSachSapXep[i];
                        DanhSachSapXep[i] = DanhSachSapXep[j];
                        DanhSachSapXep[j] = temp;
                    }
                }
            }
            foreach (Teacher teacher in DanhSachSapXep)
            {
                teacher.InThongTin();
            }
        }
        public void Init()
        {
            TeacherList.Add(new Teacher(1, "ABC", "UDPM"));
            TeacherList.Add(new Teacher(2, "DEF", "KTPM"));
            TeacherList.Add(new Teacher(3, "XYZ", "UDPM"));
        }
    }
}
