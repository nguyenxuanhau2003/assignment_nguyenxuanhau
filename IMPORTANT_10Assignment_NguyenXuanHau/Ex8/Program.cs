﻿// See https://aka.ms/new-console-template for more information

using Ex8;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;

Validation validation = new Validation();
int choice;

Service service = new Service();
service.Init();
do
{
    Console.WriteLine("=========MENU =========");
    Console.WriteLine("1. Nhập danh sách đối tượng");
    Console.WriteLine("2. Xuất danh sách đối tượng");
    Console.WriteLine("3. Xuất danh sách GV ngành UDPM");
    Console.WriteLine("4. Sắp xếp đội tượng theo ngành");
    Console.WriteLine("0. Thoát");
    choice = validation.InputIntegerInRange("Chọn chức năng: ", 0, 4);
    switch (choice)
    {
        case 1:
            service.NhapDanhSachDoiTuong();
            break;
        case 2:
            service.XuatDoiTuong();
            break;
        case 3:
            service.XuatCacTeacherUDPM();
            break;
        case 4:
            service.SapXepDoiTuongTheoNganh();
            break;
        default:
            break;
    }
} while (choice > 0 && choice <= 4);
Console.ReadKey();

