﻿// See https://aka.ms/new-console-template for more information
using NPL.M.A008.Exercise;

//Optional Arguments,
Student student1 = new Student("Ninh", "KS05", "Male", DateTime.Now, 20, "Hai Duong");
Student student2 = new Student("Hau", "KS05", "Male", DateTime.Now, 20, "Ha Noi");
Student student3 = new Student("Phuc", "KS05", "Male", DateTime.Now, 20, "Hung Yen");

//Named Arguments.
Student student4 = new Student(name: "Thanh", Class: "KS05", gender: "FEMale", entryDate: DateTime.Now, age: 20, address: "Thanh Hoa", mark: 6, grade: "B", relationship: "Single");
Student student5 = new Student(name: "Long", Class: "KS05", gender: "Male", entryDate: DateTime.Now, age: 20, address: "Nghe An", mark: 10, grade: "A", relationship: "Single");
Student student6 = new Student(name: "Tuan", Class: "KS05", gender: "Male", entryDate: DateTime.Now, age: 20, address: "Hai Duong", mark: 6, grade: "B", relationship: "Single");

//Graduate method transmission parameter
Console.WriteLine("Graduate method transmission parameter:");
Console.WriteLine(student1.Graduate(6M));

//Graduate method no parameter transmission.
Console.WriteLine("Graduate method no parameter transmission:");
Console.WriteLine(student2.Graduate());
Console.WriteLine();
//toString method no parameter transmission.
Console.WriteLine("ToString method no parameter transmission: ");
Console.WriteLine(String.Format("{0, -15}{1, -15}{2, -15}{3, -20}{4, -15}{5, -15}", "Name", "Class", "Gender", "Relationship", "Age", "Grade"));
Console.WriteLine(student1.ToString());
Console.WriteLine(student2.ToString());
Console.WriteLine(student3.ToString());


//toString method transmission transmission.
Console.WriteLine("ToString method no parameter transmission: ");
Console.WriteLine(String.Format("{0, -15}{1, -15}{2, -15}{3, -20}{4, -15}{5, -15}", "Name", "Class", "Gender", "Relationship", "Age", "Grade"));
Console.WriteLine(student4.ToString(student4.Name, student1.Class, student1.Gender, student1.Relationship, student1.Age, student1.Grade));
Console.WriteLine(student5.ToString(student5.Name, student1.Class, student1.Gender, student1.Relationship, student1.Age, student1.Grade));
Console.WriteLine(student6.ToString(student6.Name, student1.Class, student1.Gender, student1.Relationship, student1.Age, student1.Grade));


// The benefits of using Optional Arguments:
// Gán giá trị mặc định cho 1 tham số khi không được truyền giá trị vào tham số đó
// Ví dụ: student1 không gán giá trị cho grade nên student1 có grade = 'F' (F là giá trị mặc định)

// Tiện hơn trong việc gán giá trị cho các tham số

// The benefits of using Named Arguments:
// Không cần nhớ thứ tự các tham số trong constrcutor vẫn có thể khởi tạo được
//Ví dụ: Student student6 = new Student(Age: 21 ,Name:"F",Gender: "Nam", Class:"KS05",Relationship: "Single");
//       dù constructor là  Student (name, class, gender, age, relationship, mark, grade)
// Cách khai báo rõ ràng hơn

// The rules when using Optional Arguments
// Các đối số tự chọn có gắn giá trị mặc định ở sau cùng các dối số bắt buộc
// Ví dụ: public Student (string Name, string Class, string Gender, int Age, string Relationship = "Single", decimal Mark = 0, string Grade = "F")

// the rules when using Named Arguments:
// Khi trộn lẫn các đối số dùng Named Arguments và không dùng Named Arguments thì bắt buộc phải đúng thứ tự:
// Ví dụ đúng: Student student7 = new Student("G", "KS05", "Nữ", Age:24, Relationship: "Maried", 10);
// Ví dụ sai: Student student7 = new Student("KS05", "H", "Nữ", Relationship: "Maried", 10, Age:24);
