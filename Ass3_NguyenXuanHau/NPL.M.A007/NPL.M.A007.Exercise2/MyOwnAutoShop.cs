﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class MyOwnAutoShop
    {
         Sedan sedan = new Sedan(200, 3000, "Red", 182);
         Ford ford1 = new Ford(100, 2000, "Black", 2023, 200);
         Ford ford2 = new Ford(100, 2000, "Blue", 2019, 180);
         Truck truck1 = new Truck(180, 2300, "Blue", 200);
         Truck truck2 = new Truck(200, 2340, "Red", 200) ;
         public void DisplayPrice()
        {
            sedan.DisplayPrice();
            ford1.DisplayPrice();
            ford2.DisplayPrice();
            truck1 .DisplayPrice();
            truck2.DisplayPrice();
        }
    }
}
