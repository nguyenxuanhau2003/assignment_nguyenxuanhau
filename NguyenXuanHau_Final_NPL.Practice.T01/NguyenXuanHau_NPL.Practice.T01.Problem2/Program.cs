﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace NPL.Practice.T01.Problem02
{
    class Program
    {
        
        static void Main(string[] args)
        {

            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.WriteLine("---------------Test Cases----------");
            List<string> employeeNamesTestCases = new List<string>
            {
                "Ngô Việt Hùng",
                "Nguyễn Xuân Hậu",
                "Hậu",
                "Nguyễn Hải Nam",
                "Hau"
            };
            Console.WriteLine("-----Names of employees:------");
            foreach (string e in employeeNamesTestCases)
            {
                Console.WriteLine(e);
            }

            List<string> emailTestCases = GenerateEmailAddress(employeeNamesTestCases);

            Console.WriteLine("------Generated Emails:-------");
            foreach (string email in emailTestCases)
            {
                Console.WriteLine(email);
            }

            Console.WriteLine("------Test your own case:-------");
            
            List<String> employeeNames = new List<string>();

            do
            {
                employeeNames.Add(InputString("Input employee name: ", "^[a-zA-ZÀ-ỹ\\s]+$"));
            } while (InputYesNo("Do you want to input more? (Y/y or N/n)"));

            Console.WriteLine("-----Generated Emails:-------");
            // In ra emails:
            List<string> emails = GenerateEmailAddress(employeeNames);
            foreach (string email in emails)
            {
                Console.WriteLine(email);
            }

        }
        

        static private List<string> GenerateEmailAddress(List<string> listEmployees)
        {
            // bao gồm base email và số lượng mà base email này có
            Dictionary<string, int> emailCounts = new Dictionary<string, int>();

            // List danh sách kết quả email
            List<string> emailAddresses = new List<string>();

            foreach (string employeeName in listEmployees)
            {
                // Chuyển tên nhân viên thành chữ thường và tách thành từng phần
                string[] parts = employeeName.ToLower().Split(' ');
                string baseEmail = "";
                //Nếu tên nhập vào có nhiều hơn hoặc bằng 2 chữ
                if (parts.Length >= 2)
                {
                    // Lấy tên của thằng nhân viên
                    string firstName = parts[parts.Length - 1];
                    // Lấy chữ cái đầu của những tên còn lại
                    string otherCharacter = null;
                    for (int i = 0; i < parts.Length - 1; i++) {
                        otherCharacter += parts[i].Trim()[0];
                    }
                    baseEmail = firstName + otherCharacter;
                    baseEmail = RemoveDiacritics(baseEmail);
                }
                else
                {
                    baseEmail = RemoveDiacritics(employeeName).ToLower();
                    
                }
                //xem thằng base email này đã được tạo bao nhiêu lần rồi
                //nếu đã được tồn lại trước đó thì thì mình thêm 1 vào số lượng base email
                if (emailCounts.ContainsKey(baseEmail))
                    {
                        int count = emailCounts[baseEmail];
                    baseEmail = baseEmail + count;
                    count++;
                        emailCounts[baseEmail] = count;
                        
                    }
                    // nếu baseEmail chưa tồn tại thì mình thêm vào Dictionary
                    else
                    {
                        emailCounts.Add(baseEmail, 1);
                    }

                    string email = baseEmail+ "@fsoft.com.vn";
                    emailAddresses.Add(email);
                
                // Nếu tên nhân viên chỉ có một chữ, mình lấy luôn tên thằng đó thôi
               
            }

            return emailAddresses;
        }

        // Hàm validate chuỗi nhập vào xem thoả mãn regex không
        static private string InputString(string message, string pattern)
        {
            string input = string.Empty;
            while (true)
            {
                Console.Write(message);
                input = Console.ReadLine().Trim();
                if (!Regex.IsMatch(input, pattern) || input.Equals(""))
                {
                    Console.WriteLine("Please input to match regex");
                    continue;
                }
                else
                {
                    break;
                }
            }

            return input;
        }

        // Hàm validate số dương nhập vào
        static private bool InputYesNo(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                string input = Console.ReadLine();
                if (input.ToLower().Equals("y"))
                {
                    return true;
                }
                else if (input.ToLower().Equals("n"))
                {
                    return false;
                }
                else
                {
                    Console.WriteLine("Input Y/y or N/n!");
                }
            }
        }

        // Hàm loại bỏ dấu của một chuỗi Tiếng Việt
        private static string RemoveDiacritics(string input)
        {
            string normalizedString = input.Normalize(NormalizationForm.FormD);
            StringBuilder stringBuilder = new StringBuilder();

            foreach (char c in normalizedString)
            {
                if(c == 'đ' || c == 'Đ')
                {
                    stringBuilder.Append('d');
                }
                else if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                    stringBuilder.Append(c);
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }
    }
}
