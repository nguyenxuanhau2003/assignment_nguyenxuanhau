﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NguyenXuanHau_NPL.Practice.T01.Problem3
{
    internal class PhoneBook
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Group { get; set; }

        public PhoneBook() { }
        public PhoneBook(string name, string phoneNumber, string email, string address, string group)
        {
            Name = name;
            PhoneNumber = phoneNumber;
            Email = email;
            Address = address;
            Group = group;
        }

        public override string? ToString()
        {
            return string.Format("{0, -20}{1, -20}{2, -20}{3, -20}{4, -20}", Name, PhoneNumber, Email, Address, Group);
        }
    }
}
