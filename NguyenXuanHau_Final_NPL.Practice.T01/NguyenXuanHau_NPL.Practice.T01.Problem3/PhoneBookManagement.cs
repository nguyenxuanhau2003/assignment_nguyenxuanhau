﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NguyenXuanHau_NPL.Practice.T01.Problem3
{
    internal class PhoneBookManagement
    {
        List<PhoneBook> PhoneBookList = new List<PhoneBook>();
        Validation validation = new Validation();
        public void AddPhoneBook()
        {
            do
            {
                string name = validation.InputString("Input name: ", "^[a-zA-ZÀ-ỹ0-9\\s]+$");
                string phone = validation.InputString("Input phone number: ", "^0[0-9]{2} [0-9]{3} [0-9]{4}$");
                string email = validation.InputString("Input email: ", "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$");
                Console.Write("Input Address: ");
                string address = Console.ReadLine();
                string group = validation.InputGroup();
                PhoneBookList.Add(new PhoneBook(name, phone, email, address, group));
            } while (validation.InputYesNo("Do you want to input more? (Y/y or N/n)?: "));
        }

        public void RemovePhoneBook()
        {
            bool check = false;
            string name = validation.InputString("Input name of phone book to remove:", "^[a-zA-ZÀ-ỹ0-9\\s]+$");
            for (int i = 0; i < PhoneBookList.Count; i++)
            {
                if (PhoneBookList[i].Name.ToLower() == name.ToLower())
                {
                    check = true;
                    PhoneBookList.RemoveAt(i);
                }
            }
            if (check == false)
            {
                Console.WriteLine("Cannot find this phone book!");
            }
            else
            {
                Console.WriteLine("Remove successfully!");
            }
        }

        public void SortPhoneBookByName()
        {

            List<PhoneBook> listToSort = new List<PhoneBook>();
            foreach (PhoneBook phoneBook in PhoneBookList)
            {
                listToSort.Add(phoneBook);
            }
            
            listToSort.Sort((pb1, pb2) => pb1.Name.CompareTo(pb2.Name));
            Console.WriteLine("Sorted list");
            Console.WriteLine(string.Format("{0, -20}{1, -20}{2, -20}{3, -20}{4, -20}", "Name", "Phone Number", "Email", "Address", "Group"));
            foreach (PhoneBook phone in listToSort)
            {
                Console.WriteLine(phone.ToString());
            }
        }

        public void FindPhoneBookByName()
        {
            int check = 0;
            string name = validation.InputString("Input name phone to find: ", "^[a-zA-ZÀ-ỹ0-9\\s]+$");
            List<PhoneBook> result = PhoneBookList.Where(x => x.Name.ToLower().Contains(name.ToLower())).ToList();
            if (result.Count > 0)
            {
                Console.WriteLine(string.Format("{0, -20}{1, -20}{2, -20}{3, -20}{4, -20}", "Name", "Phone Number", "Email", "Address", "Group"));
                foreach (PhoneBook phone in result)
                {
                    Console.WriteLine(phone.ToString());
                }
            }
            else
            {
                Console.WriteLine("Cannot find any phone book!");
            }

        }

        public void Display()
        {
            Console.WriteLine(string.Format("{0, -20}{1, -20}{2, -20}{3, -20}{4, -20}", "Name", "Phone Number", "Email", "Address", "Group"));
            foreach (PhoneBook phonebook in PhoneBookList)
            {
                Console.WriteLine(phonebook.ToString());
            }
        }

        public void Init()
        {
            PhoneBookList.Add(new PhoneBook("Nguyen Xuan Hau", "097 407 1317", "hau@gmail.com", "Hoa Lac", "Friend"));
            PhoneBookList.Add(new PhoneBook("Nguyen Phuc Linh", "097 407 1317", "hau@gmail.com", "Hoa Lac", "Friend"));
            PhoneBookList.Add(new PhoneBook("Nguyen Nam Hai", "097 407 1317", "hau@gmail.com", "Hoa Lac", "Friend"));
            PhoneBookList.Add(new PhoneBook("Nguyen Ha Nam", "097 407 1317", "hau@gmail.com", "Hoa Lac", "Friend"));
            PhoneBookList.Add(new PhoneBook("Nguyen Ha Phuc", "097 407 1317", "hau@gmail.com", "Hoa Lac", "Friend"));
            PhoneBookList.Add(new PhoneBook("Nguyen Quy Duong", "097 407 1317", "hau@gmail.com", "Hoa Lac", "Friend"));
        }
    }
}
