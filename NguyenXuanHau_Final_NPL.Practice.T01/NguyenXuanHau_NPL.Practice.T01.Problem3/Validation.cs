﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NguyenXuanHau_NPL.Practice.T01.Problem3
{
    internal class Validation
    {
        public string InputString(string message, string pattern)
        {
            string input = string.Empty;
            while (true)
            {
                try
                {
                    Console.Write(message);
                    input = Console.ReadLine().Trim();
                    if (!Regex.IsMatch(input, pattern) || input.Equals(""))
                    {
                        Console.WriteLine("Please input to match regex");
                        throw new Exception();
                    }
                    else
                    {
                        break;
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine("Input value is not correct!");
                }
            }

            return input;

        }

        public string InputGroup()
        {
            Console.Write("Input group: ");
            while (true)
            {
                try
                {
                    string group = Console.ReadLine();
                    if (!group.Equals("Family") && !group.Equals("Colleague") && !group.Equals("Friend") && !group.Equals("Other"))
                    {
                        throw new Exception();
                    }
                    return group;
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Input value is not correct!");
                }
            }
        }

        public int InputIntegerInRange(string message, int min, int max)
        {
            string input;
            int result;
            while (true)
            {
                try
                {
                    Console.Write(message);
                    input = Console.ReadLine();
                    result = Convert.ToInt32(input);
                    if (result < min || result > max)
                    {
                        throw new Exception();
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Input value is not correct!");
                }
            }
            return result;
        }


        public bool InputYesNo(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                string input = Console.ReadLine();
                if (input.ToLower().Equals("y"))
                {
                    return true;
                }
                else if (input.ToLower().Equals("n"))
                {
                    return false;
                }
                else
                {
                    Console.WriteLine("Input Y/y or N/n!");
                }
            }
        }
    }
}
