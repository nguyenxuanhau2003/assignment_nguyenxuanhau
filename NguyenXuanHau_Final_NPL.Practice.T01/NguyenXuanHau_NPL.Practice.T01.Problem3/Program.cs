﻿// See https://aka.ms/new-console-template for more information
using NguyenXuanHau_NPL.Practice.T01.Problem3;

Console.OutputEncoding = System.Text.Encoding.Unicode;
Console.InputEncoding = System.Text.Encoding.Unicode;
int choice;
Validation validation = new Validation();
PhoneBookManagement phoneBookManagement = new PhoneBookManagement();

phoneBookManagement.Init();
do
{
    Console.WriteLine("-------MENU-----");
    Console.WriteLine("Phone Book Management");
    Console.WriteLine("1.Add Phone Book");
    Console.WriteLine("2.Remove Phone Book By Name");
    Console.WriteLine("3.Sort Phone Book By Name");
    Console.WriteLine("4.Find Phone Book By Name");
    Console.WriteLine("5.Display");
    Console.WriteLine("0. Exit.");
    choice = validation.InputIntegerInRange("Input your choice: ", 0, 5);

    switch (choice)
    {
        case 1:
            phoneBookManagement.AddPhoneBook();
            break;
        case 2:
            phoneBookManagement.RemovePhoneBook();
            break;
        case 3:
            phoneBookManagement.SortPhoneBookByName();
            break;
        case 4:
            phoneBookManagement.FindPhoneBookByName();
            break;
        case 5:
            phoneBookManagement.Display();
            break;
        case 0:
            Console.WriteLine("Program exit!");
            break;
        default:
            Console.WriteLine("Don't have choice!");
            break;

    }

}
while (choice > 0 && choice <= 5);
