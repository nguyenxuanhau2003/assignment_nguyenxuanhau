﻿internal class Program
{
    
    private static void Main(string[] args)
    {
        int[] arrTest = { 1, 1, 2, 3 };
        decimal[] resultTest = DrawCircleChart(arrTest);
        Console.WriteLine("-------------Test Cases------------");
        Console.WriteLine("Test Array: ");
        foreach (int num in arrTest)
        {
            Console.Write(num + " ");
        }
        Console.WriteLine("\nResult:");

        foreach (decimal per in resultTest)
        {
            Console.Write(per + " ");
        }


        Console.WriteLine("\n------Test your own case:-------");
        int length = InputPostiveInteger("Input the length of array: ");
        int[] arr = new int[length];

        Console.WriteLine("Input element of array:");
        for (int i = 0; i < length; ++i)
        {
            arr[i] = InputPostiveInteger("Input " + (i + 1) + "th number of the array: ");
        }
        decimal[] result = DrawCircleChart(arr);
        Console.WriteLine("The result after change to percentage: ");
        foreach (decimal r in result)
        {
            Console.Write(r + " ");
        }
    }
    private static decimal[] DrawCircleChart(int[] chartInput)
    {
        // Tạo mảng lưu phần trăm sau khi đã chuyển đổi
        decimal[] percentages = new decimal[chartInput.Length];
        int sum = chartInput.Sum();
        for (int i = 0; i < chartInput.Length; ++i)
        {
            percentages[i] = (decimal)chartInput[i] / (decimal)sum * 100;
            percentages[i] = Math.Round(percentages[i], 2);
        }

        // Do đôi khi sau khi chia tỷ lệ xong, tổng phần trăm của các phần tử trong lớp không đủ 100
        // Nên tìm phần trăm của thằng cuối cùng trong list nhập vào
        // Sao cho luôn đủ 100%

        decimal lastElementValue = 100;
        for (int i = 0; i < percentages.Length - 1; ++i)
        {
            lastElementValue -= percentages[i];
        }
        percentages[percentages.Length - 1] = lastElementValue;

        return percentages;
    }

    static private int InputPostiveInteger(string message)
    {
        string input;
        int result;
        while (true)
        {
            try
            {
                Console.Write(message);
                input = Console.ReadLine();
                result = Convert.ToInt32(input);
                if (result <= 0)
                {
                    Console.WriteLine("Input bumber > 0!");
                    continue;
                }
                else
                {
                    break;
                }
            }
            catch (Exception ex)
            {
                Console.Write("Input a number");
            }
        }
        return result;
    }
}