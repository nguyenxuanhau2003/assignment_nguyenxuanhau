﻿// See https://aka.ms/new-console-template for more information
Console.Write("Input first number: ");
int a = Convert.ToInt32(Console.ReadLine());

Console.Write("Input second number: ");
int b = Convert.ToInt32(Console.ReadLine());

Console.WriteLine("GCD: " + FindGCD(a, b));

int FindGCD(int a, int b)
{
    int temp;
    while (a != b)
    {
        if (a > b)
        {
            temp = a - b;
            a = b;
            b = temp;
        }
        else
        {
            temp = b - a;
            b = a;
            a = temp;
        }
    }
    return a;
}