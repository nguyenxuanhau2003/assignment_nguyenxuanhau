﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Input the number of the array: ");
int length = Convert.ToInt32(Console.ReadLine());
Console.WriteLine("Input the array");
int[] arr = new int[length];
for (int i = 0; i < length; i++)
{
    Console.Write("arr[" + i + "]: = ");
    arr[i] = Convert.ToInt32(Console.ReadLine());
}
int gcd = FindGCD(arr[0], arr[1]);
for(int i = 1; i < length; i++)
{
    gcd = FindGCD(gcd, arr[i]);
}

Console.WriteLine("GCD: "+gcd);

int FindGCD(int a, int b)
{
    while (a != b)
    {
        if (a > b)
        {
            int temp = a - b;
            a = b;
            b = temp;
        }
        else
        {
            int temp = b - a;
            b = a;
            a = temp;
        }
    }
    return a;
}