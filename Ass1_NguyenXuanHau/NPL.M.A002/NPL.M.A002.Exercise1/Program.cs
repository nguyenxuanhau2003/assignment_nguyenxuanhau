﻿// See https://aka.ms/new-console-template for more information

Console.WriteLine("Input the number of the array: ");
int length = Convert.ToInt32(Console.ReadLine());
Console.WriteLine("Input the array");
int[] arr = new int[length];
for (int i = 0; i < length; i++)
{
    Console.Write("arr["+i+"]: = ");
    arr[i] = Convert.ToInt32(Console.ReadLine());
}
int min = arr[0];
int max = arr[0];

for(int i = 1; i < length; i++)
{
    if (arr[i] < min)
    {
        min = arr[i];
    }
    if (arr[i] > max)
    {
        max = arr[i];
    }
}

Console.WriteLine("Min: " + min);
Console.WriteLine("Max: " + max);