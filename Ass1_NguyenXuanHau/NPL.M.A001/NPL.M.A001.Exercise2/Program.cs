﻿//Q2
Console.Write("Insert the number: ");
int number = Convert.ToInt32(Console.ReadLine());
Console.WriteLine("The binary form: ");
Console.WriteLine(ConvertToBinary(number));
String ConvertToBinary(int number)
{
    String result = "";
    while (number > 0)
    {
        result += (number % 2);
        number /= 2;
    }
    result.Reverse();
    return result;
}
