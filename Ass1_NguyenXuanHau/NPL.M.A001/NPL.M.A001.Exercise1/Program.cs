﻿//Q1
Console.WriteLine("Program to solve polynomial:");
Console.WriteLine("ax^2 + bx + c = 0");
Console.Write("a = ");
double a = Convert.ToDouble(Console.ReadLine());
Console.Write("b = ");
double b = Convert.ToDouble(Console.ReadLine());
Console.Write("c = ");
double c = Convert.ToDouble(Console.ReadLine());

if (a == 0)
{
    if (b == 0)
    {
        if (c == 0)
        {
            Console.WriteLine("There are infinite solutions");
        }
        else
        {
            Console.WriteLine("There are no solution");
        }
    }
    else
    {
        Console.WriteLine("There are one solution: x1 = " + (-c / b));
    }
}
else
{
    double delta = Math.Pow(b, 2) - 4 * a * c;
    if (delta < 0)
    {
        Console.WriteLine("No solution");
    }
    if (delta == 0)
    {
        Console.WriteLine("There are one solution: ");
        Console.WriteLine("x1 = x2 = " + (-b / (2 * a)));
    }
    else
    {
        Console.WriteLine("There are two solutions: ");
        Console.WriteLine("x1 = " + (-b - Math.Sqrt(delta)) / (2 * a));
        Console.WriteLine("x2 = " + (-b + Math.Sqrt(delta)) / (2 * a));
    }
}