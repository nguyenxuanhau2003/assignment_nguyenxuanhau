﻿Console.Write("Enter a number: ");
int n = Convert.ToInt32(Console.ReadLine());
PrintFibonacci(n);

void PrintFibonacci(int n)
{
    int num1 = 1;
    int num2 = 1;
    if (n <= 0)
    {
        Console.WriteLine("Insert n > 0");
    }
    if (n == 1)
    {
        Console.WriteLine(num1);
    }
    if (n == 2)
    {
        Console.WriteLine(num1);
        Console.WriteLine(num2);
    }
    if (n > 2)
    {
        PrintFibonacci(2);
        for (int i = 3; i <= n; i++)
        {
            int temp = num2;
            num2 = num2 + num1;
            num1 = temp;
            Console.WriteLine(num2);
        }

    }
}
